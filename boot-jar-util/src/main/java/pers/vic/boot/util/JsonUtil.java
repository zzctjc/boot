package pers.vic.boot.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections4.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/** 
 * @description: 
 * @author: Vic.xu
 * @date: 2019年11月15日 下午5:40:53
 */
public class JsonUtil {

	public static ObjectMapper objectMapper = new ObjectMapper();
	
	static {
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
	}
	
	public static String toJson(Object object) {
		if(object == null) {
			return null;
		}
		try {
			return objectMapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}
	}
	public static void printJson(Object object) {
		if(object == null) {
			return;
		}
		System.out.println(object.getClass().getSimpleName() + "\n\t" +toJson(object));
	}
	
	public static void printJson(List<?> list) {
		Optional.ofNullable(list).orElse(new ArrayList<>()).forEach(o -> {
			printJson(o);
		});
	}
}
