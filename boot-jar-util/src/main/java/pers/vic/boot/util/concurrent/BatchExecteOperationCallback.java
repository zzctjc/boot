package pers.vic.boot.util.concurrent;

import java.util.List;

/** 
 * @description: 批量操作回调
 * @author: Vic.xu
 * @date: 2020年3月11日 下午1:16:03
 */
public abstract class BatchExecteOperationCallback<T> {
	
	/**
	 * 
	 * @description: 回调
	 * @author: Vic.xu
	 * @date: 2020年3月11日 下午1:16:53
	 * @param partition 切片后的list
	 */
	public abstract void callback(List<T> partition);
}
