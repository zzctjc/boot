package per.vic.attachment.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import per.vic.attachment.model.AjaxResponse;
import per.vic.attachment.model.Attachment;
import per.vic.attachment.model.EditormdResponse;
import per.vic.attachment.service.AttachmentService;

/** 
 * @description:  附件处理控制类
 * @author: Vic.xu
 * @date: 2019年12月11日 下午3:41:45
 */
@Controller
@RequestMapping("/attachment")
public class AttachmentController {
	
	@Resource
	private AttachmentService attachmentService;
	
	/**
	 * 异步文件上传
	 * @param file 文件
	 * @param module 所属模块
	 * @return
	 */
	@PostMapping(value = "/upfile")
	@ResponseBody
	public AjaxResponse upfile(@RequestParam("upfile") MultipartFile file,
			@RequestParam(required = false, defaultValue = "none") String module) {
		AjaxResponse response = new AjaxResponse();
		if (file.isEmpty()) {
			response.setCode(HttpStatus.NO_CONTENT.value());// 204
			response.setMsg("没有上传任何文件");
			return response;
		}
		try {
			Attachment attachment = attachmentService.upfile(file, module);
			response.setData(attachment);
		} catch (IOException e) {
			e.printStackTrace();
			response.setCode(500);
			response.setMsg(e.getMessage());
		}
		return response;
	}
	
	/**
	 * 访问一个附件
	 * 
	 * @return
	 */
	@RequestMapping(value = "/visit/{id}", method = RequestMethod.GET)
	public String visit(@PathVariable int id) {
		Attachment attachment = attachmentService.selectAttachmentById(id);

		if (attachment != null && !StringUtils.isBlank(attachment.getRelativePath())) {
			return "redirect:" + attachmentService.getHost() + attachment.getRelativePath();
		}
		return "redirect:" + attachmentService.getBrokenAttachment();

	}
	
	/**
	 * 流下载文件
	 */
	@RequestMapping(value = "/download/{id}", method = RequestMethod.GET)
	public void get(HttpServletResponse response, @PathVariable int id) {
		Attachment attachment = attachmentService.selectAttachmentById(id);
		try {
			File file = attachment.getRealFile();
			if (file == null) {
				throw new IOException();
			}
			response.setContentType(attachment.getContentType());
			response.setHeader("Content-disposition", "attachment; filename=\"" + file.getName() + "\"");
			FileCopyUtils.copy(new FileInputStream(file), response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Editor.md 编辑器图片上传 TODO editormd-image-file
	 * @return
	 */
	@CrossOrigin
	@PostMapping(value = "/mdload")
	@ResponseBody
	public EditormdResponse mdload(@RequestParam("editormd-image-file") MultipartFile file) {
		if (file.isEmpty()) {
			return EditormdResponse.error("没有上传任何文件");
		}
		try {
			String module = "Editor-md";
			Attachment attachment = attachmentService.upfile(file, module);
			return EditormdResponse.success(attachment.getUrl());
		} catch (IOException e) {
			e.printStackTrace();
			return EditormdResponse.error(e.getMessage());
		}
	}

}
