package per.vic.attachment.autoconfigure;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/** 
 * @description: 附件配置项
 * @author: Vic.xu
 * @date: 2019年12月11日 上午9:33:21
 */
@Configuration
@ConfigurationProperties(prefix = AttachmentProperties.ATTACHMENT_PREFIX)
public class AttachmentProperties {
	
	public static final String ATTACHMENT_PREFIX = "attachment";
	
	public static final String DEFAULT_ATTACHMENT_REG = "/attachment/(download|visit)/(\\d+)\"";
	/**
	 * 附件存放的位置
	 */
	private String position;
	
	/**
	 * 附件访问的地址的主机. url= host + 相对路径
	 */
	private String host;
	
	/**
	 * 空的附件的相对地址
	 */
	private String broken;
	
	/**
	 * 启动时是否检测table存在
	 */
	private boolean checkTable;
	
	/**
	 * 从富文本的附件中获取附件对应的id
	 */
	public static  String textAttachmentReg = DEFAULT_ATTACHMENT_REG;
	
	/**
	 * 是否开启定时清除临时态附件
	 */
	private boolean cleaner = true;

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getBroken() {
		return broken;
	}

	public void setBroken(String broken) {
		this.broken = broken;
	}

	public boolean isCleaner() {
		return cleaner;
	}

	public void setCleaner(boolean cleaner) {
		this.cleaner = cleaner;
	}

	public String getTextAttachmentReg() {
		return textAttachmentReg;
	}

	@SuppressWarnings("static-access")
	public void setTextAttachmentReg(String textAttachmentReg) {
		if(StringUtils.isNotBlank(textAttachmentReg)) {
			this.textAttachmentReg = textAttachmentReg;
		}
	}

	public boolean isCheckTable() {
		return checkTable;
	}

	public void setCheckTable(boolean checkTable) {
		this.checkTable = checkTable;
	}
	
	
}
