package per.vic.attachment.model;

import java.io.Serializable;

/** 
 * @description: 文件上传返回对象  同 BaseResponse
 * @author: Vic.xu
 * @date: 2019年12月11日 下午3:48:28
 */
public class AjaxResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 *  0 成功 其他失败
	 */
	private int code = 0;
	
	/**
	 * 失败的时候的提示信息
	 */
	private String msg;
	
	/**
	 * 数据
	 */
	private Object data;

	public int getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

	public Object getData() {
		return data;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	
}
