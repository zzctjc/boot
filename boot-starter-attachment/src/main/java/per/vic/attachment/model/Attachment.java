package per.vic.attachment.model;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 附件表 实体类
 * 
 * @author Vic.xu
 */
public class Attachment implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer id;

	/**
	 * 绝对地址
	 */
	private String absolutePath;

	/**
	 * 相对地址 主要用于访问
	 */
	private String relativePath;

	/**
	 * 是否是临时的
	 */
	private Integer temporary;

	/**
	 * 所属模块
	 */
	private String module;

	/**
	 * mimetype
	 */
	private String contentType;

	/**
	 * 文件大小
	 */
	private Integer size;

	/**
	 * 文件名
	 */
	private String filename;

	/**
	 * 原文件名
	 */
	private String originalName;
	
	/**
	 * 创建时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd  HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	protected Date createTime;
	
	//真实文件
	private File realFile;
	
	//上传成功时设置附件的访问地址
	private String url;
	
	public Attachment() {}
	
	/**
	 * 创建附件的文件
	 * @param position: 附件存储的位置
	 * @param relativePath 相对路径
	 * @param filename 文件名
	 * @throws IOException 
	 */
	public Attachment(String position,String relativePath, String filename ) throws IOException {
		StringBuffer sb = new StringBuffer(position).append(File.separator).append(relativePath);
		File dir = new File(sb.toString());
		if(!dir.exists()) {//创建文件夹
			dir.mkdirs();
		}
		File realFile = new File(dir, filename);
		realFile.createNewFile();//创建文件
		this.absolutePath = realFile.getAbsolutePath();
		this.relativePath =(relativePath + File.separator + filename).replace(File.separator, "/");
		this.realFile = realFile;
		this.filename = filename;
	}

	/***************** set|get start **************************************/
	
	public File getRealFile() {
		if(realFile == null && StringUtils.isNotBlank(absolutePath)){
			return new File(absolutePath);
		}
		return realFile;
	}

	public void setRealFile(File realFile) {
		this.realFile = realFile;
	}
	
	
	/**
	 * set：绝对地址
	 */
	public Attachment setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
		return this;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * get：绝对地址
	 */
	public String getAbsolutePath() {
		return absolutePath;
	}

	/**
	 * set：相对地址
	 */
	public Attachment setRelativePath(String relativePath) {
		this.relativePath = relativePath;
		return this;
	}

	/**
	 * get：相对地址
	 */
	public String getRelativePath() {
		return relativePath;
	}

	/**
	 * set：是否是临时的
	 */
	public Attachment setTemporary(Integer temporary) {
		this.temporary = temporary;
		return this;
	}

	/**
	 * get：是否是临时的
	 */
	public Integer getTemporary() {
		return temporary;
	}

	/**
	 * set：所属模块
	 */
	public Attachment setModule(String module) {
		this.module = module;
		return this;
	}

	/**
	 * get：所属模块
	 */
	public String getModule() {
		return module;
	}

	/**
	 * set：mimetype
	 */
	public Attachment setContentType(String contentType) {
		this.contentType = contentType;
		return this;
	}

	/**
	 * get：mimetype
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * set：文件大小
	 */
	public Attachment setSize(Integer size) {
		this.size = size;
		return this;
	}

	/**
	 * get：文件大小
	 */
	public Integer getSize() {
		return size== null ? 0 :size;
	}

	/**
	 * set：文件名
	 */
	public Attachment setFilename(String filename) {
		this.filename = filename;
		return this;
	}

	/**
	 * get：文件名
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * set：原文件名
	 */
	public Attachment setOriginalName(String originalName) {
		this.originalName = originalName;
		return this;
	}

	/**
	 * get：原文件名
	 */
	public String getOriginalName() {
		return originalName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
	
	
	/***************** set|get end **************************************/
}
