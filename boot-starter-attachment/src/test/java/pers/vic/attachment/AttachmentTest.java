package pers.vic.attachment;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.assertj.core.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import per.vic.attachment.dao.AttachmentDao;
import per.vic.attachment.model.Attachment;

/** 
 * @description: 测试附件
 * @author: Vic.xu
 * @date: 2019年12月11日 上午10:55:17
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Rollback(true)  
public class AttachmentTest {
	
	ObjectMapper mapper = new ObjectMapper();

	@Resource
	AttachmentDao attachmentDao;
	
	//1. insert
	@Test
	public void insert() throws JsonProcessingException {
		Attachment a = new Attachment();
		a.setAbsolutePath("4567");
		a.setRelativePath("8888");
		a.setContentType("xx888");
		a.setFilename("test88");
		a.setOriginalName("ooo88oo");
		attachmentDao.insert(a);
		System.out.println(mapper.writeValueAsString(a));
	}
	//2. query
	@Test
	public void query() throws JsonProcessingException {
		Attachment a  = attachmentDao.selectAttachmentById(3);
		System.out.println(mapper.writeValueAsString(a));
	}
	//3. list
	@Test
	public void list() throws JsonProcessingException {
		List<Attachment> list  = attachmentDao.selectTemporaryAttachments();
		System.out.println(mapper.writeValueAsString(list));
	}
	
	//4 update
	@Test
	public void update() {
		attachmentDao.updateTemporary(1, false);
		attachmentDao.updateTemporary("1,2,3", true);
	}
	
	@Test
	public void delete() {
		attachmentDao.delete(1);
		
		attachmentDao.delete(Arrays.asList("2,3".split(",")).stream().map(s->Integer.parseInt((String) s)).collect(Collectors.toList()));
	}
}
