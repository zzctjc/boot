package pers.vic.boot.base.service;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.pagehelper.PageHelper;

import per.vic.attachment.service.AttachmentService;
import pers.vic.boot.base.lookup.Lookup;
import pers.vic.boot.base.mapper.BaseMapper;
import pers.vic.boot.base.model.BaseEntity;
import pers.vic.boot.base.model.PageInfo;
/**
 * Service基类
 * @author VIC.xu
 * @param <M> 继承自BaseMapper的Mapper接口
 * @param <T> 当前service对应的实体
 * @date 2019-11-12 
 */
public abstract class BaseService<M extends BaseMapper<T>, T extends BaseEntity> {

	@Autowired
	protected M mapper;
	
	@Resource
	private AttachmentService attachmentService;
	
	/**
	 * 当前实体是否包含附件
	 * @return
	 */
	protected abstract boolean hasAttachment();
	
	public void startPage(int pageNum, int pageSize){
		PageHelper.startPage(pageNum, pageSize);
	}

	public void setMapper(M mapper) {
		this.mapper = mapper;
	}

	/** 查询列表 */
	public List<T> list(Lookup lookup) {
		List<T> datas = mapper.list(lookup);
		return datas;
	}
	/** 查询分页列表 */
	public PageInfo<T> page(Lookup lookup) {
		startPage(lookup.getPage(), lookup.getSize());
		List<T> datas = mapper.list(lookup);
		return PageInfo.instance(datas);
	}

	/** 根据主键id查询对象 */
	public T findById(int id) {
		return mapper.findById(id);
	}

	/** 插入对象 */
	public int insert(T entity) {
		if(hasAttachment()) {
			attachmentService.addAttachmengFromObj(entity);
		}
		return mapper.insert(entity);
	}

	/** 更新数据 */
	public int update(T entity) {
		if(hasAttachment()) {
			T old = findById(entity.getId());
			attachmentService.handleOldAndNowAttachment(old, entity);
		}
		return mapper.update(entity);
	}
	
	/**保存:根据id判断新增或更新实体*/
	public int save(T entity) {
		if(entity.getId() == null || entity.getId() <= 0) {
			return insert(entity);
		}else {
			return update(entity);
		}
	}
	
	/**根据id删除记录*/
	public int delete(int id) {
		if(hasAttachment()) {
			attachmentService.deleteAttachmengFromObj(findById(id));
		}
		return this.delete(new int[] {id});
	}

	/** 批量删除 */
	public int delete( int[] ids) {
		if(hasAttachment()) {
			attachmentService.deleteAttachmengFromObj(findByIds(ids));
		}
		return mapper.delete(ids);
	}

	/** 批量获取 */
	public List<T> findByIds(@Param("ids") int[] ids) {
		return mapper.findByIds(ids);
	}

}
