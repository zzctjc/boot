package pers.vic.boot.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import pers.vic.boot.base.lookup.Lookup;

public interface BaseMapper<T>{

	/**查询列表*/
	List<T> list(Lookup lookup);
	
	/**根据主键id查询对象*/
	T findById(@Param("id") int id);
	
	/**插入对象*/
	int insert(T entity);
	
	/**更新数据*/
	int update(T entity);
	
	/**批量删除*/
	int  delete(@Param("ids")int[] ids);
	
	/**批量获取*/
	List<T> findByIds(@Param("ids")int[] ids);
	
}