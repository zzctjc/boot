package pers.vic.boot.base.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import pers.vic.boot.base.model.BaseEntity;
import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.base.model.PageInfo;
import pers.vic.boot.base.service.BaseService;

/**
 * @description: controller 的基类
 * @author: Vic.xu
 * @date: 2019年11月13日 下午2:40:41
 */
public abstract class BaseController<S extends BaseService<?, T>, T extends BaseEntity> {

	@Autowired
	protected S service;

	@Resource
	private HttpServletRequest request;

	/**
	 * 列表
	 * 
	 * @return
	 */
	@GetMapping(value = "list")
	public BaseResponse list(T lookup) {
		PageInfo<T> list = service.page(lookup);
		return BaseResponse.success(list);
	}

	/**
	 * 保存
	 * @param entity
	 * @return
	 */
	@PostMapping(value = "/save")
	public BaseResponse save(T entity) {
		service.save(entity);
		return BaseResponse.success(entity);
	}

	/**
	 * 详情
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping(value = "/detail")
	public BaseResponse detail(Integer id) {
		T entity = service.findById(id);
		return BaseResponse.success(entity);
	}

	@PostMapping(value = "/delete")
	public BaseResponse delete(int id) {
		service.delete(id);
		return BaseResponse.success();
	}
	
	@PostMapping(value = "/batchDelete")
	public BaseResponse delete(@RequestParam("ids[]")int[] ids) {
		service.delete(ids);
		return BaseResponse.success();
	}

}
