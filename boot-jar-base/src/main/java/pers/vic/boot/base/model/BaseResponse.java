package pers.vic.boot.base.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 返回JSON的基本格式  
 * @author VIC
 *
 */
public final class BaseResponse {

	private static final Logger logger = LoggerFactory.getLogger(BaseResponse.class);

	/**
	 * 状态码 0为正确
	 */
	private int code;

	/**
	 * 错误提示信息
	 */
	private String msg;

	/**
	 * 数据对象
	 */
	private Object data;
	
	
	public static BaseResponse judge(boolean judge) {
		return judge ? success() : error();
	}

	/** 构建操作成功的对象 */
	public static BaseResponse success(Object data) {
		return new BaseResponse(0, "", data);
	}

	/** 构建操作成功的对象 */
	public static BaseResponse success() {
		return new BaseResponse(0, "");
	}

	public static BaseResponse error() {
		return error(999, "");
	}
	
	/** 构建操作失败的对象 */
	public static BaseResponse error(String msg) {
		return error(999, msg);
	}

	/** 构建操作失败的对象 */
	public static BaseResponse error(int code, String msg) {
		return new BaseResponse(code, msg);
	}

	/**
	 * 转JSON
	 */
	public String toJson() {
		try {
			return mapper.writeValueAsString(this);
			
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}
		return "{" + "\"code\":" + code + ",\"msg\":\"" + msg + "\",\"data\":\"" + data + "\"}";
	}

	/**
	 * 返回结果
	 *//*
	public String result() {
		if (FinalFiledParams.START_ENCRYPT) {
			return this.toAesJson();
		}
		return this.toJson();
	}*/

	/**
	 * 转JSON并加密
	 *//*
	public String toAesJson() {
		String result = null;
		try {
			result = AESUtil.encrypt(toJson());
		} catch (Exception e) {
			logger.error("返回json，加密异常", e);
		}
		return result;
	}*/

	@Override
	public String toString() {
		return this.toJson();
	}

	public BaseResponse() {
	}

	private BaseResponse(int code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}

	private BaseResponse(int code, String msg, Object data) {
		super();
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public BaseResponse appendMsg(String msg) {
		this.msg += msg;
		return this;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public BaseResponse setMsg(String msg) {
		this.msg = msg;
		return this;
	}

	public Object getData() {
		return data;
	}

	public BaseResponse setData(Object data) {
		this.data = data;
		return  this;
	}

	private static ObjectMapper mapper = new ObjectMapper();
	
	

}
