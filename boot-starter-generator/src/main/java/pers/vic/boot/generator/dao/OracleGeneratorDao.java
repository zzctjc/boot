package pers.vic.boot.generator.dao;

import java.util.List;

import pers.vic.boot.generator.model.ColumnEntity;
import pers.vic.boot.generator.model.TableEntity;

/** 
 * @description: 
 * @author: Vic.xu
 * @date: 2020年3月10日 上午11:19:20
 */
public class OracleGeneratorDao extends GeneratorDao {

	@Override
	public List<TableEntity> queryList(TableEntity lookup) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ColumnEntity> queryColumns(String tableName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TableEntity queryTable(String tableName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DatabaseType databaseType() {
		return DatabaseType.oracle;
	}

	@Override
	public int countList(TableEntity lookup) {
		// TODO Auto-generated method stub
		return 0;
	}
}
