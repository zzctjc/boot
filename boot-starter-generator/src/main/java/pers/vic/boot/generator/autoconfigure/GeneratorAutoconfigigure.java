package pers.vic.boot.generator.autoconfigure;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.Assert;

import pers.vic.boot.generator.dao.GeneratorDao;
import pers.vic.boot.generator.dao.GeneratorDao.DatabaseType;

/**
 * @description: 代码生成starter 入口
 * @author: Vic.xu
 * @date: 2020年3月10日 上午10:54:39
 */
@Configuration
@ConditionalOnClass(value = { JdbcTemplate.class })
@ConditionalOnWebApplication
@ConditionalOnExpression("${generatir.enabled:true}")
@EnableConfigurationProperties(GeneratorProperties.class)
public class GeneratorAutoconfigigure {

	/**
	 * 本项目中支持的各个数据库类型
	 */
	@Autowired
	private Collection<GeneratorDao> generatorDaoList;
	
	/**
	 * 当前配置的数据库类型 默认MYSQL
	 */
	@Value("${generator.database:mysql}")
	private String database;

	@Primary
	@Bean
	public GeneratorDao getGeneratorDao() {
		DatabaseType type = DatabaseType.getByName(database);
		Assert.notNull(type, "当前项目未支持" + database + "类型的数据库代码生成");
		GeneratorDao dao = null;
		for(GeneratorDao generatorDao : generatorDaoList) {
			if(type == generatorDao.databaseType()) {
				dao = generatorDao;
				break;
			}
		}
		Assert.notNull(dao, "当前项目未实现" + database + "类型的数据库的GeneratorDao");
		
		return dao;
	}

}
