package pers.vic.boot.generator.autoconfigure;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/** 
 * @description: 附件配置项
 * @author: Vic.xu
 * @date: 2019年12月11日 上午9:33:21
 */
@Configuration
@ConfigurationProperties(prefix = GeneratorProperties.GENERATOR_PREFIX)
@PropertySource("classpath:generator-config.properties")
@JsonIgnoreProperties({"beanExpressionResolver"})
public class  GeneratorProperties implements Serializable{
	private static final long serialVersionUID = 1L;

	public static final String GENERATOR_PREFIX = "generator";
	
	/**
	 * 包名
	 */
	private String packageName;
	
	/**
	 * 包名
	 */
	private String moduleName;
	
	/**
	 * 作者
	 */
	private String author;
	
	/**
	 * email
	 */
	private String email;
	
	/**
	 * 表前缀 若存在则生成的实体名会去掉前缀
	 */
	private String tablePrefix;
	
	/**
	 * 实体中忽略的表字段
	 */
	private Set<String> ignores;
	
	/**
	 * 需要填充的数据模板
	 */
	private List<String> templates;
	
	/**
	 * 数据类型转换 数据库类型--> java 类型
	 */
	private Map<String, String> dataTypeConvert;
	

	@Override
	public String toString() {
		return "GeneratorProperties [packageName=" + packageName + ",\n moduleName=" + moduleName + ",\n author=" + author
				+ ",\n email=" + email + ",\n tablePrefix=" + tablePrefix + ",\n ignores=" + ignores + ",\n templates="
				+ templates + ",\n dataTypeConvert=" + dataTypeConvert + "]";
	}

	public String getPackageName() {
		return packageName;
	}

	public String getModuleName() {
		return moduleName;
	}

	public String getAuthor() {
		return author;
	}

	public String getEmail() {
		return email;
	}

	public String getTablePrefix() {
		return tablePrefix;
	}

	public Set<String> getIgnores() {
		return ignores;
	}
	
	

	public List<String> getTemplates() {
		return templates;
	}

	public Map<String, String> getDataTypeConvert() {
		return dataTypeConvert;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setTablePrefix(String tablePrefix) {
		this.tablePrefix = tablePrefix;
	}

	public void setIgnores(Set<String> ignores) {
		this.ignores = ignores;
	}

	public void setTemplates(List<String> templates) {
		this.templates = templates;
	}

	public void setDataTypeConvert(Map<String, String> dataTypeConvert) {
		this.dataTypeConvert = dataTypeConvert;
	}
	
	
	
	
	
	
	
	
}
