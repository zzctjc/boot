package pers.vic.boot.generator.controller;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.base.model.PageInfo;
import pers.vic.boot.generator.model.TableConfigVO;
import pers.vic.boot.generator.model.TableEntity;
import pers.vic.boot.generator.service.GeneratorService;

/**
 * 自动生成代码
 * 
 * @author VIC
 */
@RestController
@RequestMapping("/generator")
public class GeneratorController {

	@Resource
	private HttpServletResponse response;

	@Resource
	private GeneratorService generatorService;

	/**
	 * 数据库表列表
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping(value = "/list")
	public BaseResponse list(TableEntity lookup) {
		PageInfo<TableEntity> data = generatorService.list(lookup);
		return BaseResponse.success(data);
	}


	/**
	 * 查询表的列的情况
	 * 
	 * @param tableName
	 * @return
	 */
	@GetMapping(value = "/table/detail")
	public BaseResponse table(@RequestParam(value = "id") String tableName) {
		TableEntity data = generatorService.buildTableDetail(tableName);
		return BaseResponse.success(data);
	}


	/**
	 * 根据条件导出
	 */
	@RequestMapping(value = "/exportByConfig", method = { RequestMethod.POST, RequestMethod.GET })
	public void exportByConfig(TableConfigVO tableConfigVO) throws IOException {
		byte[] data = generatorService.exportByConfig(tableConfigVO);
		download(data);
	}

	private void download(byte[] data) throws IOException {
		response.reset();
		response.setHeader("Content-Disposition", "attachment; filename=\"generator-vic.zip\"");
		response.addHeader("Content-Length", "" + data.length);
		response.setContentType("application/octet-stream; charset=UTF-8");

		IOUtils.write(data, response.getOutputStream());
	}

}
