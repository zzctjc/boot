package pers.vic.boot.generator;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import pers.vic.boot.generator.autoconfigure.GeneratorProperties;


/**
 * @description: 自动生成代码的starter
 * @author VIC.xu
 * @date: 2020年3月10日 上午10:50:28
 */
@SpringBootApplication
public class GeneratorApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(GeneratorApplication.class, args);
	}
	
	@Resource
	private GeneratorProperties generatorProperties;
	
	@PostConstruct
	public void post() {
		System.out.println(generatorProperties);
	}
}
