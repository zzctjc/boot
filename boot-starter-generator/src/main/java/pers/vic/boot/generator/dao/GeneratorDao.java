package pers.vic.boot.generator.dao;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;

import pers.vic.boot.generator.model.ColumnEntity;
import pers.vic.boot.generator.model.TableEntity;

/**
 * @description: 查询DAO，本来准备使用MYBATIS的，但是改为Starter之后，不想引入额外的依赖
 * @author: Vic.xu
 * @date: 2020年3月10日 上午11:11:35
 */
public abstract class GeneratorDao {
	
	@Resource
	protected JdbcTemplate jdbcTemplate;
	
	/**
	 * 数据库名称
	 */
	public abstract DatabaseType databaseType();

	/**
	 * 查询表列表
	 */
	public abstract List<TableEntity> queryList(TableEntity lookup);
	
	/**
	 * 查询表列表总数 用于分页
	 */
	public abstract int countList(TableEntity lookup);

	/**
	 * 查询表的所有列的列表
	 */
	public abstract List<ColumnEntity> queryColumns(String tableName);

	/**
	 * 查询表的详情
	 */
	public abstract TableEntity queryTable(String tableName);
	
	
	/**数据库*/
	public enum DatabaseType {
		mysql,oracle;
		
		public static DatabaseType getByName(String name) {
			for(DatabaseType databaseType : DatabaseType.values()) {
				if(databaseType.name().equals(name)) {
					return databaseType;
				}
			}
			return null;
		}
	}
	
	public static void main(String[] args) {
		DatabaseType type = DatabaseType.getByName("mysql");
		System.out.println(type);
	}
}
