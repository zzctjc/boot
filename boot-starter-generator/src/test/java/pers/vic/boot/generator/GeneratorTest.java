package pers.vic.boot.generator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import pers.vic.boot.base.model.PageInfo;
import pers.vic.boot.generator.dao.GeneratorDao;
import pers.vic.boot.generator.model.ColumnConfigVO;
import pers.vic.boot.generator.model.TableConfigVO;
import pers.vic.boot.generator.model.TableEntity;
import pers.vic.boot.generator.service.GeneratorService;
import pers.vic.boot.util.JsonUtil;

/**
 * test
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Rollback(true)
public class GeneratorTest {

	@Test
	public void contextLoads() {
	}

	@Resource
	GeneratorDao generatorDao;

	@Resource
	GeneratorService generatorService;

	/**
	 * 查询列表
	 * 
	 * @description:
	 * @author: Vic.xu
	 * @date: 2020年3月13日 下午3:31:47
	 */
	@Test
	public void list() {
		String tableName="";
		TableEntity lookup = new TableEntity();
		lookup.setTableName(tableName);
		PageInfo<TableEntity> page = generatorService.list(lookup);
		System.err.println("总数据：" + page.getTotal());
		System.err.println("总页数：" + page.getPages());
		System.err.println("当前页：" + page.getPage());
		System.err.println("当前页数据：");
		page.getDatas().forEach(TableEntity::printLine);

	}
	@Test
	public void detail() {
		String tableName = "zbl_form_data";
		TableConfigVO vo = detail(tableName);
		JsonUtil.printJson(vo);
	}

	@Test
	public void export() {
		String path = "C:/Users/Administrator/Desktop/generator";
		String fileName = "generator.zip";
		String[] tableNames = {"t_auth_application",
				"t_auth_org_indentify"
				,"t_auth_orgnization"
				,"t_auth_position"
				,"t_auth_position_indentify"
				,"t_auth_user"
				};
		for(String tableName : tableNames) {
			TableConfigVO vo = detail(tableName);
			byte[] data = generatorService.exportByConfig(vo);
			try {
				FileUtils.writeByteArrayToFile(new File(path, tableName + "_"+ fileName ), data);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		

	}
	
	public TableConfigVO detail(String tableName) {
		TableEntity entity = generatorService.buildTableDetail(tableName);
		JsonUtil.printJson(entity);
		String packageName = "com.kjlink";
		String moduleName = "auth";
		TableConfigVO vo = new TableConfigVO(tableName, packageName, moduleName);
		List<ColumnConfigVO> columns = new ArrayList<ColumnConfigVO>();
		Optional.ofNullable(entity.getColumns()).orElse(new ArrayList<>()).forEach(c->{
			Integer condition = "string".equalsIgnoreCase(c.getAttrType()) ? 1 :0;
			ColumnConfigVO columnConfigVO = new ColumnConfigVO(c.getColumnName(), condition, true);
			columns.add(columnConfigVO);
		});;
		vo.setColumns(columns);
		JsonUtil.printJson(vo);
		return vo;
	}
}