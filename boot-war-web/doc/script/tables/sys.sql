/**
 * 系统模块(system)的表
 */

/* 001.菜单表 */
CREATE TABLE `sys_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `code` bigint(18) NOT NULL  COMMENT '按钮编码,从100开始每级别占三位,同级别递增',
  `name` varchar(64) DEFAULT NULL COMMENT '菜单名称',
  `icon` varchar(64) DEFAULT NULL COMMENT '菜单图标',
  `pid` int(11) DEFAULT '0' COMMENT '所属父类',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型 1-目录 2-菜单 3-按钮',
  `url` varchar(128) DEFAULT NULL COMMENT '菜单url',
  `permission` varchar(128) DEFAULT NULL COMMENT '权限标识',
  `sort` int(8) DEFAULT NULL COMMENT '排序',
  `is_enable` tinyint(1) DEFAULT '1' COMMENT '是否启用',
  `is_delete` tinyint(1) DEFAULT '1' COMMENT '是否删除',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';

/* 002.系统用户表 */
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(64) DEFAULT NULL COMMENT '昵称',
  `username` varchar(64) DEFAULT NULL COMMENT '登录名',
  `password` varchar(128) DEFAULT NULL COMMENT '密码',
  `gender` tinyint(1) DEFAULT '0' COMMENT '性别 0-保密 1-男2-女',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `email` varchar(128) DEFAULT NULL COMMENT '邮箱',
  `avatar` int(11) DEFAULT NULL COMMENT '头像,对应附件表id',
  `phone` char(11) DEFAULT NULL COMMENT '电话',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  `is_enable` tinyint(1) DEFAULT '1' COMMENT '是否启用',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='系统用户表';


/* 003. 数据字典表*/
CREATE TABLE `sys_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父字典id',
  `code` varchar(64) DEFAULT NULL COMMENT '编码',
  `sys_code` varchar(512) DEFAULT '' COMMENT '系统编码:pcode-code(方便删除和查找子节点)',
  `name` varchar(64) DEFAULT NULL COMMENT '字典名称',
  `value` varchar(64) DEFAULT NULL COMMENT '字典值',
  `sort` int(8) DEFAULT NULL COMMENT '排序',
  `remark` varchar(128) DEFAULT NULL COMMENT '说明',
  `is_enable` tinyint(1) DEFAULT '1' COMMENT '是否启用',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据字典表';

/* 004. 系统角色表*/
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(64) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(128) DEFAULT NULL COMMENT '角色备注',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  `is_enable` tinyint(1) DEFAULT '1' COMMENT '是否启用',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

/*  005. 角色菜单关系表 */
CREATE TABLE `sys_role_menu_rel` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id;1;',
  `role_id` INT(11) NOT NULL COMMENT '角色id;1;',
  `menu_id` INT(11) NOT NULL COMMENT '菜单id;1;',
  PRIMARY KEY (`id`)
) ENGINE=INNODB   DEFAULT CHARSET=utf8 COMMENT='角色菜单关系表';

/*  006. 用户角色关系表 */
CREATE TABLE `sys_user_role_rel` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id;1;',
  `role_id` INT(11) NOT NULL COMMENT '角色id;1;',
  `user_id` INT(11) NOT NULL COMMENT '用户id;1;',
  PRIMARY KEY (`id`)
) ENGINE=INNODB   DEFAULT CHARSET=utf8 COMMENT='用户角色关系表';
