/**
 * 001. 开发计划表
 */
CREATE TABLE `develop_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(128) NOT NULL COMMENT '计划名称',
  `start_date` date DEFAULT NULL COMMENT '开始时间',
  `end_date` date DEFAULT NULL COMMENT '结束时间',
  `progress` int(3) DEFAULT '0' COMMENT '进度百分比',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态:0-未开始 1-待定,2-进行中,3-暂停,4-完成',
  `remark` text COMMENT '说明',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '开始时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_enable` tinyint(1) DEFAULT '1' COMMENT '是否启用',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='开发计划';
