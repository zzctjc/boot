package pers.vic.boot.console.generator.config;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

//import org.springframework.context.annotation.ImportResource;

@PropertySource("classpath:config/generator-config.properties")
@ConfigurationProperties()
@Component
public class ConfigurationOfGenerator {
	
	//包名
	public String packageName;
	
	public String moduleName;
	//作者
	public String author;
	//Email
	public String email;
	
	//表前缀(类名不会包含表前缀)
	public String tablePrefix;
	
	/**
	 * 在实体中忽略的字段(存在于基类)
	 */
	public Set<String> ignores;
	
	/**
	 * 需要填充的模板
	 */
	public List<String> templates;
	
	
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTablePrefix() {
		return tablePrefix;
	}
	public void setTablePrefix(String tablePrefix) {
		this.tablePrefix = tablePrefix;
	}
	public List<String> getTemplates() {
		return templates;
	}
	public void setTemplates(List<String> templates) {
		this.templates = templates;
	}

	public Set<String> getIgnores() {
		return ignores == null ? new HashSet<String>() : ignores;
	}

	public void setIgnores(Set<String> ignores) {
		this.ignores = ignores;
	}
	
	


	
	

}
