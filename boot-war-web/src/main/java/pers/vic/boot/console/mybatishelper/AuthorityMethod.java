package pers.vic.boot.console.mybatishelper;

import pers.vic.boot.console.mybatishelper.AuthorityData.FilterColumn;

/**
 * @description: 权限拦截器的基类
 * @author: Vic.xu
 * @date: 2020年2月11日 下午3:58:58
 */
public abstract class AuthorityMethod {
	
	/**
	 * 当前线程中需要拦截的权限字段数据
	 */
	protected static final ThreadLocal<AuthorityData> LOCAL_AUTHORITY_DATA = new ThreadLocal<AuthorityData>();
	
    /**
     * 设置 AuthorityData 参数
     *
     * @param AuthorityData
     */
    protected static void setLocalAuthorityData(AuthorityData authorityData) {
        LOCAL_AUTHORITY_DATA.set(authorityData);
    }

    /**
     * 获取 AuthorityData 参数
     *
     * @return
     */
    public static AuthorityData getLocalAuthorityData() {
        return LOCAL_AUTHORITY_DATA.get();
    }

    /**
     * 移除本地变量
     */
    public static void clearAuthorityData() {
        LOCAL_AUTHORITY_DATA.remove();
    }
    
    /**
     * 
     * @description: 开启某个类型的权限拦截
     * @author: Vic.xu
     * @date: 2020年2月11日 下午4:20:41
     * @param type  类型
     * @param column 对应字段
     */
    public static void start(AuthorityType type, String column) {
    	AuthorityData data = getLocalAuthorityData();
    	if(data == null) {
    		data = new AuthorityData();
    	}
    	FilterColumn filterColumn = new FilterColumn(type, column);
    	data.addFilterColumn(filterColumn);
    	setLocalAuthorityData(data);
    }
    
    public static void main(String[] args) {
    	AuthorityMethod.start(AuthorityType.DEPT, "a.type");
    	AuthorityMethod.start(AuthorityType.DEPT, "a.dept");
    	AuthorityMethod.start(AuthorityType.TEST, "b.test");
    	AuthorityData data = getLocalAuthorityData();
    	data.getFilterColumns().forEach(a->{
    		System.out.println(a.getColumn() + "  " + a.getType());
    	});
    	
	}
}
