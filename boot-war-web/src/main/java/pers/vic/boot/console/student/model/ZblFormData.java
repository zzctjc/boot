package pers.vic.boot.console.student.model;


import pers.vic.boot.base.model.BaseEntity;


/**
 * 表单数据填写结果表 实体类
 * 
 * @author Vic.xu
 */
public class ZblFormData extends BaseEntity {
	
	private static final long serialVersionUID = 1L;

								/**
	 * 所属form
	 */
	 							    private Integer formId;
	
						/**
	 * 学生姓名
	 */
	 							    private String studentName;
	
						/**
	 * 学号
	 */
	 							    private String studentNo;
	
						/**
	 * 表单的数据  应为json
	 */
	 							    private String data;
	
																			
    /***************** set|get  start **************************************/
			    	    	    	/**
	 * set：所属form
	 */
	public ZblFormData setFormId(Integer formId) {
		this.formId = formId;
		return this;
	}
    /**
	* get：所属form
	*/
		    public Integer getFormId() {
		return formId;
	}
		    	    	/**
	 * set：学生姓名
	 */
	public ZblFormData setStudentName(String studentName) {
		this.studentName = studentName;
		return this;
	}
    /**
	* get：学生姓名
	*/
		    public String getStudentName() {
		return studentName;
	}
		    	    	/**
	 * set：学号
	 */
	public ZblFormData setStudentNo(String studentNo) {
		this.studentNo = studentNo;
		return this;
	}
    /**
	* get：学号
	*/
		    public String getStudentNo() {
		return studentNo;
	}
		    	    	/**
	 * set：表单的数据  应为json
	 */
	public ZblFormData setData(String data) {
		this.data = data;
		return this;
	}
    /**
	* get：表单的数据  应为json
	*/
		    public String getData() {
		return data;
	}
		    	    	    	    	    	    	    	    	        /***************** set|get  end **************************************/
}
