package pers.vic.boot.console.student.mapper;

import pers.vic.boot.base.mapper.BaseMapper;
import pers.vic.boot.console.student.model.ZblStudent;
/**
 * @description:学号表-学生唯一标识 并用于登陆 Mapper
 * @author Vic.xu
 * @date: 2020-02-02 18:13
 */
public interface ZblStudentMapper extends BaseMapper<ZblStudent> {
	
}
