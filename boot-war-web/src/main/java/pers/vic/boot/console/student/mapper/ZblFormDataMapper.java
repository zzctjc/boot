package pers.vic.boot.console.student.mapper;

import pers.vic.boot.base.mapper.BaseMapper;
import pers.vic.boot.console.student.model.ZblFormData;
/**
 * @description:表单数据填写结果表 Mapper
 * @author Vic.xu
 * @date: 2020-02-02 18:13
 */
public interface ZblFormDataMapper extends BaseMapper<ZblFormData> {
	
}
