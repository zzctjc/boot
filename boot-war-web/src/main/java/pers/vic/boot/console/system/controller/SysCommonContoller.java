package pers.vic.boot.console.system.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.console.system.service.SysCommonService;
import pers.vic.boot.console.system.vo.Select2VO;

/** 
 * @description: 系统模块的一些通用的请求
 * @author: Vic.xu
 * @date: 2019年12月3日 上午11:26:30
 * TODO  走缓存
 */
@RestController
@RequestMapping("/system/common")
public class SysCommonContoller {
	
	@Resource
	private SysCommonService sysCommonService;
	
	/**
	 * 角色下拉框
	 * @return
	 */
	@PostMapping(value = "roleSelect")
	public BaseResponse roleSelect() {
		List<Select2VO> data = sysCommonService.roleSelect();
		return BaseResponse.success(data);
	}
	
	/**
	 * 字典下拉框
	 * 查看一个编码字典的子项
	 */
	@PostMapping(value = "dictSelect")
	public BaseResponse dictSelect(String pcode) {
		List<Select2VO> data = sysCommonService.dictSelect(pcode);
		return BaseResponse.success(data);
	}
	

}
