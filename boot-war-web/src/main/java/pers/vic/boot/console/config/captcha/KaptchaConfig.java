package pers.vic.boot.console.config.captcha;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;

import sun.misc.BASE64Encoder;

/**
 * @description: 验证码配置类
 * @author: Vic.xu
 * @date: 2020年2月18日 上午11:21:38
 */
@Configuration
public class KaptchaConfig {
	/**
	 * 验证码长度
	 */
	private int codeLength  = 4;

	@Bean
	public DefaultKaptcha producer() {

		Properties properties = new Properties();
		properties.put("kaptcha.border", "no");
		properties.put("kaptcha.textproducer.font.color", "black");
		properties.put("kaptcha.textproducer.char.space", "10");
		properties.put("kaptcha.textproducer.char.length", "4");
		properties.put("kaptcha.image.height", "34");
		properties.put("kaptcha.textproducer.font.size", "25");

		properties.put("kaptcha.noise.impl", "com.google.code.kaptcha.impl.NoNoise");
		Config config = new Config(properties);
		DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
		defaultKaptcha.setConfig(config);
		return defaultKaptcha;
	}

	@Bean
	@Primary
	public DefaultKaptcha getKaptchaBean() {
		DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
		Properties properties = new Properties();
		properties.setProperty("kaptcha.border", "no");// 图片边框
		properties.setProperty("kaptcha.border.color", "105,179,90");// 边框颜色
		properties.setProperty("kaptcha.textproducer.font.color", "black");// 字体颜色
		properties.setProperty("kaptcha.image.width", "120");// 宽
		properties.setProperty("kaptcha.image.height", "38");// 高
		properties.setProperty("kaptcha.session.key", "code");
		properties.setProperty("kaptcha.textproducer.char.length", String.valueOf(codeLength));// 验证码长度
		properties.setProperty("kaptcha.textproducer.font.size", "25");//字体大小
		properties.setProperty("kaptcha.textproducer.char.space", "12");
		properties.setProperty("kaptcha.textproducer.font.names", "宋体,楷体,微软雅黑");
		//干扰实现类
		properties.put("kaptcha.noise.impl", "com.google.code.kaptcha.impl.DefaultNoise");
		//干扰颜色
		properties.put("kaptcha.noise.color", "124,252,0");
		properties.setProperty("kaptcha.obscurificator.impl", "com.google.code.kaptcha.impl.WaterRipple");
		Config config = new Config(properties);
		defaultKaptcha.setConfig(config);
		return defaultKaptcha;
	}

	/*
	 * 参见com.google.code.kaptcha.Constants 
	 * 
	 * 默认配置 
	 * kaptcha.border 图片边框，合法值：yes , no yes
	 * kaptcha.border.color 边框颜色，合法值： r,g,b (and optional alpha) 或者
	 * white,black,blue. black kaptcha.border.thickness 边框厚度，合法值：>0 1
	 * kaptcha.image.width 图片宽 200 kaptcha.image.height 图片高 50 kaptcha.producer.impl
	 * 图片实现类 com.google.code.kaptcha.impl.DefaultKaptcha kaptcha.textproducer.impl
	 * 文本实现类 com.google.code.kaptcha.text.impl.DefaultTextCreator
	 * kaptcha.textproducer.char.string 文本集合，验证码值从此集合中获取 abcde2345678gfynmnpwx
	 * kaptcha.textproducer.char.length 验证码长度 5 kaptcha.textproducer.font.names 字体
	 * Arial, Courier 
	 * kaptcha.textproducer.font.size 字体大小 40px.
	 * kaptcha.textproducer.font.color 字体颜色，合法值： r,g,b 或者 white,black,blue. black
	 * kaptcha.textproducer.char.space 
	 * 文字间隔 2 
	 * kaptcha.noise.impl 干扰实现类
	 * com.google.code.kaptcha.impl.DefaultNoise 
	 * kaptcha.noise.color 
	 * 干扰颜色，合法值： r,g,b
	 * 或者 white,black,blue. black 
	 * kaptcha.obscurificator.impl 
	 * 图片样式：
	 * 水纹com.google.code.kaptcha.impl.WaterRipple
	 * 鱼眼com.google.code.kaptcha.impl.FishEyeGimpy 阴影
	 * 
	 * com.google.code.kaptcha.impl.ShadowGimpy
	 * com.google.code.kaptcha.impl.WaterRipple 
	 * kaptcha.background.impl 背景实现类
	 * com.google.code.kaptcha.impl.DefaultBackground 
	 * kaptcha.background.clear.from
	 * 背景颜色渐变，开始颜色 light grey kaptcha.background.clear.to 背景颜色渐变，结束颜色 white
	 * kaptcha.word.impl 文字渲染器 com.google.code.kaptcha.text.impl.DefaultWordRenderer
	 * kaptcha.session.key session key KAPTCHA_SESSION_KEY kaptcha.session.date
	 * session date KAPTCHA_SESSION_DATE
	 */
}
