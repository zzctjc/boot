package pers.vic.boot.console.student.model;

import pers.vic.boot.base.model.BaseEntity;

/**
 * 表单设计详情表 实体类
 * 
 * @author Vic.xu
 */
public class ZblBaseFormDetail extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 所属表单的id
	 */
	private Integer formId;

	/**
	 * 字段名
	 */
	private String name;

	/**
	 * 1-input 2-select
	 */
	private Integer type;

	/**
	 * 校验 正则表达式
	 */
	private String validate;

	/**
	 * 选项的时候的选择值，多个逗号分隔
	 */
	private String typeValue;

	/**
	 * 默认值
	 */
	private String defaultValue;

	/**
	 * 错误提醒
	 */
	private String validateRemind;

	/**
	 * 是否必填
	 */
	private Integer required;

	/***************** set|get start **************************************/
	/**
	 * set：所属表单的id
	 */
	public ZblBaseFormDetail setFormId(Integer formId) {
		this.formId = formId;
		return this;
	}

	/**
	 * get：所属表单的id
	 */
	public Integer getFormId() {
		return formId;
	}

	/**
	 * set：字段名
	 */
	public ZblBaseFormDetail setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * get：字段名
	 */
	public String getName() {
		return name;
	}

	/**
	 * set：1-input 2-select
	 */
	public ZblBaseFormDetail setType(Integer type) {
		this.type = type;
		return this;
	}

	/**
	 * get：1-input 2-select
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * set：校验 正则表达式
	 */
	public ZblBaseFormDetail setValidate(String validate) {
		this.validate = validate;
		return this;
	}

	/**
	 * get：校验 正则表达式
	 */
	public String getValidate() {
		return validate;
	}

	/**
	 * set：选项的时候的选择值，多个逗号分隔
	 */
	public ZblBaseFormDetail setTypeValue(String typeValue) {
		this.typeValue = typeValue;
		return this;
	}

	/**
	 * get：选项的时候的选择值，多个逗号分隔
	 */
	public String getTypeValue() {
		return typeValue;
	}

	/**
	 * set：默认值
	 */
	public ZblBaseFormDetail setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
		return this;
	}

	/**
	 * get：默认值
	 */
	public String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * set：错误提醒
	 */
	public ZblBaseFormDetail setValidateRemind(String validateRemind) {
		this.validateRemind = validateRemind;
		return this;
	}

	/**
	 * get：错误提醒
	 */
	public String getValidateRemind() {
		return validateRemind;
	}

	/**
	 * set：是否必填
	 */
	public ZblBaseFormDetail setRequired(Integer required) {
		this.required = required;
		return this;
	}

	/**
	 * get：是否必填
	 */
	public Integer getRequired() {
		return required;
	}
	/***************** set|get end **************************************/
}
