package pers.vic.boot.console.mybatishelper;

import java.util.HashSet;
import java.util.Set;

/** 
 * @description: 存储在当前线程中用于权限处理拦截器的基本数据
 * @author: Vic.xu
 * @date: 2020年2月11日 下午3:41:07
 */
public class AuthorityData {
	
	/**
	 * 需要过滤的权限字段
	 */
	private Set<FilterColumn> filterColumns = new HashSet<AuthorityData.FilterColumn>();
	
	/**
	 * 新增要过滤的权限字段
	 */
	public AuthorityData addFilterColumn(FilterColumn column) {
		this.filterColumns.add(column);
		return this;
	}
	public Set<FilterColumn> getFilterColumns() {
		return filterColumns;
		
	}



	/**
	 * @description: 需要过滤的权限列字段; 当type一致 就认为是一个对象
	 * @author VIC.xu
	 * @date: 2020年2月11日 下午3:47:17
	 */
	static class FilterColumn {
		//权限类型
		private AuthorityType type;
		
		//过滤的列 如a.dept_id
		private String column;
		
		

		public FilterColumn(AuthorityType type, String column) {
			super();
			this.type = type;
			this.column = column;
		}
		
		

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			return result;
		}



		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FilterColumn other = (FilterColumn) obj;
			if (type != other.type)
				return false;
			return true;
		}

		public AuthorityType getType() {
			return type;
		}

		public String getColumn() {
			return column;
		}

		public void setType(AuthorityType type) {
			this.type = type;
		}

		public void setColumn(String column) {
			this.column = column;
		}

		
	}
	
}
