package pers.vic.boot.console.mybatishelper;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;

/**
 * @description: 权限过滤类型, 并在此种处理SQL的追加
 * @author: Vic.xu
 * @date: 2020年2月11日 下午3:49:00
 */
public enum AuthorityType {

	DEPT {
		public String handlerSql(String sql, String column) {
			try {
				Select select = (Select) CCJSqlParserUtil.parse(sql);
				//TODO 此处的18 应该从当前线程的token中获取
				String append = column + " like '%18' ";
				Expression where = CCJSqlParserUtil.parseCondExpression(append);
				((PlainSelect) select.getSelectBody()).setWhere(where);
				String handledSql = select.toString();
				return handledSql;
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			return sql;
		}
	},

	TEST {
		public String handlerSql(String sql, String column) {
			return sql + "test";
		}
	},
	OWER{
		public String handlerSql(String sql, String column) {
			return sql + "test";
		}	
	};
	// 这个抽象方法由不同枚举值提供不同的实现
	public abstract String handlerSql(String sql, String column);

}
