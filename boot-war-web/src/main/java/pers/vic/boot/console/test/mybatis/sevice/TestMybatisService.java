package pers.vic.boot.console.test.mybatis.sevice;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import pers.vic.boot.base.lookup.Lookup;
import pers.vic.boot.base.model.PageInfo;
import pers.vic.boot.base.service.BaseService;
import pers.vic.boot.console.mybatishelper.AuthorityHelper;
import pers.vic.boot.console.mybatishelper.AuthorityType;
import pers.vic.boot.console.test.mybatis.mapper.TestMybatisMapper;
import pers.vic.boot.console.test.mybatis.model.TestMybatis;

@Service
public class TestMybatisService extends BaseService<TestMybatisMapper, TestMybatis> {

	@Override
	protected boolean hasAttachment() {
		return false;
	}
	
	@Resource
	private ApplicationContext applicationContext;
	
	private TestMybatisService self;
	
	@PostConstruct
	public void post() {
		this.self = applicationContext.getBean(TestMybatisService.class);
	}

	public PageInfo<TestMybatis> page(Lookup lookup) {
		startPage(lookup.getPage(), lookup.getSize());
		// 测试加入权限拦截
		AuthorityHelper.start(AuthorityType.DEPT, "a.name");
		List<TestMybatis> datas = mapper.list(lookup);
		return PageInfo.instance(datas);
	}

	public List<Map<String, Object>> queryFromOracle(boolean paged) {
		if (paged) {
			startPage(1, 5);
		}
		return mapper.queryFromOracle();
	}
	

	public void testTransactional() throws InterruptedException {
		CountDownLatch countDownLatch = new CountDownLatch(2);
		new Thread(()-> {
			System.err.println("addAge1");
			countDownLatch.countDown();
			try {
				self.addAge1(15);
			}catch (Exception e) {
			}
		}).start();
		
		new Thread(()-> {
			System.err.println("addAge2");
			countDownLatch.countDown();
			try {
				self.addAge2(15);
			}catch (Exception e) {
			}
		}).start();
		
		countDownLatch.await();
		System.out.println("end..........................");
	}
	
	@Transactional(isolation = Isolation.SERIALIZABLE, rollbackFor =  Exception.class)
	public void rollback(int id) throws InterruptedException {
		TestMybatis mybatis = findById(id);
		System.err.println("age = " +mybatis.getAge());
		mybatis.setAge(mybatis.getAge() + 1);
		mapper.update(mybatis);
		throw new RuntimeException("123");
	}
	
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public void addAge1(int id) throws InterruptedException {
		System.err.println("先进入addAge111111111111111.........................................");
		TestMybatis mybatis = findById(id);
		TimeUnit.SECONDS.sleep(2);
		System.err.println("age = " +mybatis.getAge());
		mybatis.setAge(mybatis.getAge() + 1);
		mapper.update(mybatis);
		System.err.println("addAge111111111111111--------休眠2s---------------------年龄+1 后执行");
	}
	
	
	
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public void addAge2(int id) throws InterruptedException {
		System.err.println("后进入addAge2222222222222.........................................");
		TestMybatis mybatis = findById(id);
		TimeUnit.SECONDS.sleep(1);
		System.err.println("age = " +mybatis.getAge());
		mybatis.setAge(mybatis.getAge() + 2);
		mapper.update(mybatis);
		System.err.println("后进入addAge2222222222222.--------休眠1s---------------------年龄+2 先执行");
	}
	
	public void deleteTest(int ...id) {
		AuthorityHelper.start(AuthorityType.OWER, "a.user_id");
		mapper.delete(id);
	}


}
