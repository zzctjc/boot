package pers.vic.boot.console.rpc.consumer;

import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;

import pers.vic.boot.console.util.JsonUtil;
import pers.vic.boot.rpc.test.model.RpcModel1;
import pers.vic.boot.rpc.test.service.FirstRpcService;

/** 
 * @description: 
 * @author: Vic.xu
 * @date: 2019年12月10日 上午11:34:41
 */
@Service
public class TestRpcService {

	@Reference
	private FirstRpcService firstRpcService;
	
	public void test(Integer id) {
		RpcModel1 model = firstRpcService.getModel(id);
		JsonUtil.printJson(model);
		firstRpcService.deleteModel(id);
	}
}
