package pers.vic.boot.console.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import pers.vic.boot.console.system.vo.Select2VO;
/**
 * @description:系统模块的一些通用 Mapper
 * @author Vic.xu
 * @date: 2019-01-28 10:01
 */
public interface SysCommonMapper  {

	/**
	 * 全部的角色
	 * @return
	 */
	List<Select2VO> roleSelect();

	/**
	 * 查询一个字典编码的子项
	 */
	List<Select2VO> dictSelect(@Param("pcode")String pcode);
	
	
}
