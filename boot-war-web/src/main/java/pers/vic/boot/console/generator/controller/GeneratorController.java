package pers.vic.boot.console.generator.controller;

import java.io.IOException;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pers.vic.boot.base.lookup.Lookup;
import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.base.model.PageInfo;
import pers.vic.boot.console.generator.model.TableConfigVO;
import pers.vic.boot.console.generator.model.TableEntity;
import pers.vic.boot.console.generator.service.GeneratorService;

/**
 * 自动生成代码
 * 
 * @author VIC
 */
@RestController
@RequestMapping("/generator")
public class GeneratorController {

	@Resource
	private HttpServletResponse response;

	@Resource
	private GeneratorService generatorService;

	/**
	 * 数据库表列表
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping(value = "/list")
	public BaseResponse list(TableEntity lookup) {
		PageInfo<TableEntity> data = generatorService.page(lookup);
		return BaseResponse.success(data);
	}

	@RequestMapping(value = "/list2")
	public PageInfo<TableEntity> list2(Model model) {
		PageInfo<TableEntity> data = generatorService.page(new Lookup());
		return data;
	}

	/**
	 * 详情页
	 */
	@RequestMapping(value = "/list/{tableName}", method = RequestMethod.GET)
	public String detail(@PathVariable String tableName, @RequestParam(required = false) String packageName,
			@RequestParam(required = false) String moduleName, Model model) {
		Map<String, String> data = generatorService.findTableDetail(tableName, packageName, moduleName);
		model.addAttribute("data", data);
		return "generator/form";
	}

	/**
	 * 查询表的列的情况
	 * 
	 * @param tableName
	 * @return
	 */
	@GetMapping(value = "/table/detail")
	public BaseResponse table(@RequestParam(value = "id") String tableName) {
		TableEntity data = generatorService.buildTableDetail(tableName);
		return BaseResponse.success(data);
	}

	/**
	 * 导出
	 * 
	 * @throws IOException
	 */
	@RequestMapping(value = "/export", method = { RequestMethod.POST, RequestMethod.GET })
	public void export(@RequestParam String[] tableNames, @RequestParam(required = false) String packageName,
			@RequestParam(required = false) String moduleName, HttpServletResponse response) throws IOException {
		byte[] data = generatorService.generatorCode(tableNames, packageName, moduleName);
		download(data);
	}

	/**
	 * 根据条件导出
	 */
	@RequestMapping(value = "/exportByConfig", method = { RequestMethod.POST, RequestMethod.GET })
	public void exportByConfig(TableConfigVO tableConfigVO) throws IOException {
		byte[] data = generatorService.exportByConfig(tableConfigVO);
		download(data);
	}

	private void download(byte[] data) throws IOException {
		response.reset();
		response.setHeader("Content-Disposition", "attachment; filename=\"generator-vic.zip\"");
		response.addHeader("Content-Length", "" + data.length);
		response.setContentType("application/octet-stream; charset=UTF-8");

		IOUtils.write(data, response.getOutputStream());
	}

}
