package pers.vic.boot.console.develop.service;

import org.springframework.stereotype.Service;
import pers.vic.boot.base.service.BaseService;
import pers.vic.boot.console.develop.mapper.DevelopPlanMapper;  
import pers.vic.boot.console.develop.model.DevelopPlan;

/**
 * @description:开发计划 Service
 * @author Vic.xu
 * @date: 2019-53-04 09:53
 */
@Service
public class DevelopPlanService extends BaseService<DevelopPlanMapper, DevelopPlan>{

	@Override
	protected boolean hasAttachment() {
		// TODO Auto-generated method stub
		return false;
	}

}
