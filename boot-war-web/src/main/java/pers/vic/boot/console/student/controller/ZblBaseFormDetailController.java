package pers.vic.boot.console.student.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pers.vic.boot.base.controller.BaseController;
import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.console.student.model.ZblBaseFormDetail;
import pers.vic.boot.console.student.service.ZblBaseFormDetailService;

/**
 * @description:表单设计详情表 控制层
 * @author Vic.xu
 * @date: 2020-02-02 18:10
 */
@RestController
@RequestMapping("/student/zblBaseFormDetail")
public class ZblBaseFormDetailController extends BaseController<ZblBaseFormDetailService, ZblBaseFormDetail>{

	/**
	 * @description: 根据formId 查询不分页列表 
	 * @author: Vic.xu
	 * @date: 2020年2月2日 下午10:02:20
	 * @param lookup
	 * @return
	 */
	@GetMapping(value = "detailList")
	public BaseResponse list(Integer formId) {
		List<ZblBaseFormDetail> list = service.list(new ZblBaseFormDetail().setFormId(formId));
		return BaseResponse.success(list);
	}
}
