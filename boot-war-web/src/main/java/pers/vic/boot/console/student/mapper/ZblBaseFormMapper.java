package pers.vic.boot.console.student.mapper;

import pers.vic.boot.base.mapper.BaseMapper;
import pers.vic.boot.console.student.model.ZblBaseForm;
/**
 * @description:表单基本信息 Mapper
 * @author Vic.xu
 * @date: 2020-02-02 18:04
 */
public interface ZblBaseFormMapper extends BaseMapper<ZblBaseForm> {
	
}
