package pers.vic.boot.console.config.captcha;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.code.kaptcha.impl.DefaultKaptcha;

import sun.misc.BASE64Encoder;

/** 
 * @description: 验证码生成
 * @author: Vic.xu
 * @date: 2020年2月18日 下午1:47:42
 */
@Component
public class KaptchGenerate {

	@Autowired
	private DefaultKaptcha producer;
	
	/**
	 * 验证码凭证前缀
	 */
	private String token_prefix = "captcha_";
	
	/**
	 * @description: 生成验证码对象
	 * @author: Vic.xu
	 * @date: 2020年2月18日 下午1:53:40
	 * @return
	 * @throws IOException
	 */
	public Base64CaptchaModel generateWithoutToken() throws IOException {
		String token = UUID.randomUUID().toString().replace("-", "");
		token += token_prefix;
		return generateWithToken(token);
	}
	
	/**
	 * 
	 * @description: 生成验证码对象
	 * @author: Vic.xu
	 * @date: 2020年2月18日 下午1:53:56
	 * @param token 验证码凭证
	 * @return
	 * @throws IOException
	 */
	public Base64CaptchaModel generateWithToken(String token) throws IOException {
		String text = producer.createText();
		System.out.println(text);
		BufferedImage image = producer.createImage(text);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ImageIO.write(image, "jpg", outputStream);
		// 对字节数组Base64编码
		BASE64Encoder encoder = new BASE64Encoder();
		String base64Image = encoder.encode(outputStream.toByteArray());
		return new Base64CaptchaModel(token, text, "data:image/jpg;base64," + base64Image);
	}
	
	
}
