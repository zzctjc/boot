package pers.vic.boot.console.config.shiro;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.console.system.model.SysUser;
import pers.vic.boot.console.system.service.SysUserService;
import pers.vic.boot.util.CommonUtils;

/** 
 * @description:  代码的执行流程 preHandle -> isAccessAllowed -> isLoginAttempt -> executeLogin 。
 * @author: Vic.xu
 * @date: 2019年12月31日 上午11:55:08
 */
public class JwtFilter extends BasicHttpAuthenticationFilter {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Resource
    private SysUserService sysUserService;

    /**
     * 判断用户是否想要登入。
     * 检测header里面是否包含Authorization字段即可
     */
    @Override
    protected boolean isLoginAttempt(ServletRequest request, ServletResponse response) {
        HttpServletRequest req = (HttpServletRequest) request;
        String authorization = req.getHeader("Authorization");
        boolean attempt = StringUtils.isNoneBlank(authorization);
        return attempt;
    }

    /**
     *执行登陆操作
     */
    @Override
    protected boolean executeLogin(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String authorization = httpServletRequest.getHeader("Authorization");
        JwtToken token = new JwtToken(authorization);
        // 提交给realm进行登入，如果错误他会抛出异常并被捕获
        getSubject(request, response).login(token);
        //如果登陆成功在判断是否需要刷新token
        needRefreshToken(authorization, (HttpServletResponse) response,  (HttpServletRequest) request);
        return true;
    }
    
    /**
     * @description: 判断token是否将要过期 提醒前端获取新的token
     * @author: Vic.xu
     * @date: 2020年1月14日 下午2:57:51
     * @param token
     * @param response
     */
    private void needRefreshToken(String token, HttpServletResponse response, HttpServletRequest request) {
    	try {
    		AuthorityInfo info = JwtUtil.getAuthorityInfo(token);
    	    boolean need = info.needRefresh();
    	    if(!need) {
    	    	return;
    	    }
    	    //判断前端是否请求刷新token
    	    if(refreshToken(info, response,  request)) {
    	    	return;
    	    }
    	  //设置将要过期标识   让前端手动刷新token
	    	response.setHeader("willExpire ", "1");	
    	}catch(Exception e) {
    		e.printStackTrace();
    		//解token异常不处理
    	}
    }
    
    /**
     * @description: 刷新token, 在原token将要过期  以及前端手动要求刷新token的时候
     *   进入此方法之前理应登陆成功,也即用户存 账号匹配
     * @author: Vic.xu
     * @date: 2020年1月14日 下午2:59:36
     * @return
     */
    private boolean refreshToken(AuthorityInfo authorityInfo, HttpServletResponse response, HttpServletRequest request) {
    	//请求更换token标识
    	String exchangeToken = request.getHeader("exchangeToken");
    	if(StringUtils.equals("1", exchangeToken)) {
    		//生成新的token放在响应头中
    		String username = authorityInfo.getUsername();
    		if(StringUtils.isEmpty(username)) {
    			return false;
    		}
    		SysUser user = sysUserService.findUserByUsername(username);
    		String tokenNew = sysUserService.buildToken(user);
    		if(tokenNew != null) {
    			//把新的token放入响应头
    	    	response.setHeader("token ", tokenNew);	
    	    	return true;
    		}
    	}
    	return false;
    }

    /**
     * 这里我们详细说明下为什么最终返回的都是true，即允许访问
     * 例如我们提供一个地址 GET /article
     * 登入用户和游客看到的内容是不同的
     * 如果在这里返回了false，请求会被直接拦截，用户看不到任何东西
     * 所以我们在这里返回true，Controller中可以通过 subject.isAuthenticated() 来判断用户是否登入
     * 如果有些资源只有登入用户才能访问，我们只需要在方法上面加上 @RequiresAuthentication 注解即可
     * 但是这样做有一个缺点，就是不能够对GET,POST等请求进行分别过滤鉴权(因为我们重写了官方的方法)，但实际上对应用影响不大
     */
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        if (isLoginAttempt(request, response)) {
            try {
                executeLogin(request, response);
            } catch (Exception e) {
            	logger.error("executeLogin error: ", e);
                CommonUtils.writeJson(BaseResponse.error(401, "请先登陆"), (HttpServletResponse) response);
                return false;
            }
        }
        return true;
    }
    

	/**
     * 对跨域提供支持
     */
    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        httpServletResponse.setHeader("Access-control-Allow-Origin", httpServletRequest.getHeader("Origin"));
        httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PUT,DELETE");
        httpServletResponse.setHeader("Access-Control-Allow-Headers", httpServletRequest.getHeader("Access-Control-Request-Headers"));
        // 跨域时会首先发送一个option请求，这里我们给option请求直接返回正常状态
        if (httpServletRequest.getMethod().equals(RequestMethod.OPTIONS.name())) {
            httpServletResponse.setStatus(HttpStatus.OK.value());
            return false;
        }
        return super.preHandle(request, response);
    }

}