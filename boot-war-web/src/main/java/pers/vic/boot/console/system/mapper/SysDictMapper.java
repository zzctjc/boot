package pers.vic.boot.console.system.mapper;

import org.apache.ibatis.annotations.Param;

import pers.vic.boot.base.mapper.BaseMapper;
import pers.vic.boot.console.system.model.SysDict;
/**
 * @description:数据字典表 Mapper
 * @author Vic.xu
 * @date: 2019-01-28 10:01
 */
public interface SysDictMapper extends BaseMapper<SysDict> {
	
	/**
	 * 判断code是否重复
	 * @return
	 */
	boolean checkCode(@Param("id")Integer id, @Param("code")String code);

	/**根据id 删除自身以及子节点*/
	int deleteSelfAndSub(@Param("id")int id);
	
}
