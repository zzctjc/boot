package pers.vic.boot.console.config.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/** 
 * @description: 
 * @author: Vic.xu
 * @date: 2019年12月31日 上午11:49:24
 */
public class JwtToken implements AuthenticationToken {

	private static final long serialVersionUID = 1L;
	
	/**
	 *  密钥
	 */
    private String token;

    public JwtToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}