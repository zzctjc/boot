package pers.vic.boot.console.mybatishelper;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pers.vic.boot.console.mybatishelper.AuthorityData.FilterColumn;

/**
 * @description: 权限拦截器 ,  拦截StatementHandler对象的prepare ,然后重写SQL 加入where条件过滤;
 * 	使用方式: AuthorityHelper.start
 * @author: Vic.xu
 * @date: 2020年2月11日 下午1:41:39
 */
@Intercepts({@Signature(type = StatementHandler.class, method = "prepare", args = {Connection.class, Integer.class})})
public class AuthorityHelper extends AuthorityMethod implements Interceptor {
	
	private static Logger LOGGER = LoggerFactory.getLogger(AuthorityHelper.class);

	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		Object target = invocation.getTarget();
        StatementHandler statementHandler = (StatementHandler) target;
        //追加处理权限的sql
        handlerAuthoritySql(statementHandler);
		Object result = invocation.proceed();
		return result;
	}
	
	/**
	 * @description: 追加处理权限的SQL
	 * @author: Vic.xu
	 * @date: 2020年2月11日 下午3:30:41
	 * @param statementHandler
	 * @throws Throwable
	 */
	private void handlerAuthoritySql(StatementHandler statementHandler) throws Throwable {
 		BoundSql boundSql = statementHandler.getBoundSql();
        String sql = boundSql.getSql();
        AuthorityData authorityData = getLocalAuthorityData();
        if(authorityData == null) {
        	return;
        }
        
        Set<FilterColumn> columns = authorityData.getFilterColumns();
        if(CollectionUtils.isEmpty(columns)) {
        	return;
        }
        for(FilterColumn column :  columns) {
        	AuthorityType type =  column.getType();
        	//处理SQL 追加权限
        	sql = type.handlerSql(sql, column.getColumn());
        }
		System.err.println("intercept..............................\n" + sql);
		//通过反射把SQL设置回去
		Field field = boundSql.getClass().getDeclaredField("sql");
        field.setAccessible(true);
        field.set(boundSql, sql);
	}

	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}

	@Override
	public void setProperties(Properties properties) {
		String dialect = properties.getProperty("dialect");
        LOGGER.info("mybatis intercept dialect:{}", dialect);

	}

}
