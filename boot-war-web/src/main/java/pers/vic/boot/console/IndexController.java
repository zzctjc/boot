package pers.vic.boot.console;

import java.io.IOException;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.console.config.captcha.Base64CaptchaModel;
import pers.vic.boot.console.config.captcha.KaptchGenerate;
import pers.vic.boot.console.config.redis.RedisService;
import pers.vic.boot.console.system.service.SysUserService;

/** 
 * @description: 
 * @author: Vic.xu
 * @date: 2019年11月27日 上午9:34:48
 */
@Controller
public class IndexController {
	
	@Resource
	private SysUserService sysUserService;
	
	@Resource
	private KaptchGenerate kaptchGenerate;
	
	@Resource
	private RedisService redisService;
	
	//验证码有效时长 秒 (5分钟)
	public static final long CODE_DURATION = 5 * 60;

	@GetMapping(value = "/index")
	@ResponseBody
	public BaseResponse index(@RequestParam(defaultValue = "")String msg) {
		return BaseResponse.success("i am index " + msg);
	}
	
	@GetMapping(value = "/home")
	public String home(RedirectAttributes attributes) {
		attributes.addAttribute("msg", "我是从home重定向而来的信息");
		return "redirect:/index";
	}
	
	
	@PostMapping(value = "/login")
	@ResponseBody
	public BaseResponse login(String username, String password, String captcha, String token) {
		String cacheCaptcha = (String) redisService.get(token);
		if(StringUtils.isEmpty(cacheCaptcha)) {
			return BaseResponse.error("验证码错误");
		}
		
		if(!StringUtils.equals(captcha, cacheCaptcha)) {
			return BaseResponse.error("验证码错误");
		}
		BaseResponse response = sysUserService.login(username, password);
		return response;
	}
	
	/**
	 * @description: TODO 退出登录  把token置为失效：  黑名单的方式?
	 * @author: Vic.xu
	 * @date: 2020年2月17日 下午5:18:48
	 * @return
	 */
	@PostMapping(value = "/logout")
	@ResponseBody
	public BaseResponse logout() {
		String token = (String) SecurityUtils.getSubject().getPrincipal();
		// 放入REDIS 作为黑名单
		return BaseResponse.success();
	}
	
	/**
	 * @description: 获取验证码  存入REDIS
	 * @author: Vic.xu
	 * @date: 2020年2月18日 下午1:56:47
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value = "/captcha", method = {RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public BaseResponse captcha() throws IOException {
		Base64CaptchaModel captcha = kaptchGenerate.generateWithoutToken();
		String token = captcha.getToken();
		redisService.set(token, captcha.getCode(), CODE_DURATION);
		return BaseResponse.success(captcha);
	}
		
}
