package pers.vic.boot.console.student.service;

import org.springframework.stereotype.Service;
import pers.vic.boot.base.service.BaseService;
import pers.vic.boot.console.student.mapper.ZblStudentMapper;  
import pers.vic.boot.console.student.model.ZblStudent;

/**
 * @description:学号表-学生唯一标识 并用于登陆 Service
 * @author Vic.xu
 * @date: 2020-02-02 18:13
 */
@Service
public class ZblStudentService extends BaseService<ZblStudentMapper, ZblStudent>{

    @Override
    protected boolean hasAttachment() {
        return false;
    }

}
