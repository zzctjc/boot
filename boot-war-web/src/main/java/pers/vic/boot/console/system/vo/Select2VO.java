package pers.vic.boot.console.system.vo;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/** 
 * @description: 对应select2的插件的data数据
 * @author: Vic.xu
 * @date: 2019年12月3日 上午11:29:07
 */
public class Select2VO {

	/**
	 * 选项的值
	 */
	private Object id;
	/**
	 * 选项的展示
	 */
	private String text;
	
	@JsonIgnore
	private Object pid;
	
	/**
	 * 子选项
	 */
	private List<Select2VO> children = new ArrayList<Select2VO>();

	public Object getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public List<Select2VO> getChildren() {
		return children;
	}

	public void setId(Object id) {
		this.id = id;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setChildren(List<Select2VO> children) {
		this.children = children;
	}

	public Object getPid() {
		return pid;
	}

	public void setPid(Object pid) {
		this.pid = pid;
	}
	
}
