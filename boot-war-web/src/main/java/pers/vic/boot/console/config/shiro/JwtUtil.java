package pers.vic.boot.console.config.shiro;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;

/** 
 * @description: JWT 加密，校验工具
 * @author: Vic.xu
 * @date: 2019年12月31日 上午11:19:14
 */
public class JwtUtil {

    // 过期时间X分钟
    private static final long EXPIRE_TIME = 60*60*1000;
    
    //开始检测刷新token的时机
    private static final long EXPIRE_CHECK = 30*60*1000;

    /**
     * 校验token是否正确
     * @param token 密钥
     * @param secret 用户的密码
     * @return 是否正确
     */
    public static boolean verify(String token, String username, String secret) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withClaim("username", username)
                    .build();
            DecodedJWT jwt = verifier.verify(token);
            //TODO jwt的一些其他判断
            jwt.getToken();
            return true;
        } catch (Exception e) {
        	e.printStackTrace();
        	
            return false;
        }
    }

    /**
     * 获得token中的信息无需secret解密也能获得
     * @return token中包含的用户名
     */
    public static String getUsername(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("username").asString();
        } catch (JWTDecodeException e) {
            return null;
        }
    }
    
    /**
     * @description: 从当前线程中获取用户信息
     * @author: Vic.xu
     * @date: 2020年2月17日 下午4:28:13
     * @return
     */
    public static AuthorityInfo getAuthorityInfoFromShiro() {
    	String token = (String) SecurityUtils.getSubject().getPrincipal();
    	if(StringUtils.isBlank(token)) {
    		return null;
    	}
    	return getAuthorityInfo(token);
    }
    /**
     * 获得token中的信息
     * @return AuthorityInfo 
     */
    public static AuthorityInfo getAuthorityInfo(String token) {
    	try {
    		DecodedJWT jwt = JWT.decode(token);
        	return new AuthorityInfo(jwt.getClaim("userId").asInt(),
        			jwt.getClaim("username").asString(), 
        			jwt.getClaim("roles").asArray(String.class), 
        			jwt.getClaim("permission").asArray(String.class), 
        			jwt.getClaim("expireCheckTime").asLong());
    	}catch (Exception e) {
    		return new AuthorityInfo();
		}
    	
    }

    /**
     * 生成签名,X分钟后过期
     * @param username 用户名
     * @param secret 用户的密码
     * @return 加密的token
     */
    public static String sign(String username, String secret) {
        try {
            Date date = new Date(System.currentTimeMillis()+EXPIRE_TIME);
            Algorithm algorithm = Algorithm.HMAC256(secret);
            // 附带username信息
            return JWT.create()
                    .withClaim("username", username)
                    .withExpiresAt(date)
                    .sign(algorithm);
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * 生成签名,X分钟后过期
     * @param username 用户名
     * @param secret 用户的密码
     * @return 加密的token
     */
    public static String sign(AuthorityInfo authorityInfo, String secret) {
        try {
        	//开始检测是否需要更换token的时机
        	long expireCheckTime = System.currentTimeMillis()+EXPIRE_CHECK;
        	
            Date date = new Date(System.currentTimeMillis()+EXPIRE_TIME);
            Algorithm algorithm = Algorithm.HMAC256(secret);
            // 附带username信息
            return JWT.create()
            		.withClaim("userId", authorityInfo.getUserId())
                    .withClaim("username", authorityInfo.getUsername())
                    .withClaim("expireCheckTime", expireCheckTime)
                    .withArrayClaim("roles", authorityInfo.getRoleSet().toArray(new String[] {}))
                    .withArrayClaim("permission", authorityInfo.getPermissionSet().toArray(new String[] {}))
                    .withExpiresAt(date)
                    .sign(algorithm);
        } catch (Exception e) {
            return null;
        }
    }
    
    public static void main(String[] args) {
    	String username ="zhangsan";
    	String secret = "123";
    	AuthorityInfo info = new AuthorityInfo(1,"zhangsan123", "a,b,c".split(","), "ee,ff,gg".split(","), System.currentTimeMillis() + EXPIRE_CHECK);
		String token = sign(info, "123");
		System.out.println(token.length());
		System.out.println(token);
		boolean b = verify(token, username, secret);
		System.out.println("verify = " + b);
		AuthorityInfo info2 = getAuthorityInfo(token);
		System.out.println(info2);
	}
    
  
}
