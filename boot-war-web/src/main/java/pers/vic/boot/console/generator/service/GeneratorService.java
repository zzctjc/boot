package pers.vic.boot.console.generator.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.annotation.Resource;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;

import pers.vic.boot.base.lookup.Lookup;
import pers.vic.boot.base.model.PageInfo;
import pers.vic.boot.console.generator.config.ConfigurationOfGenerator;
import pers.vic.boot.console.generator.mapper.GeneratorMapper;
import pers.vic.boot.console.generator.model.ColumnConfigVO;
import pers.vic.boot.console.generator.model.ColumnEntity;
import pers.vic.boot.console.generator.model.TableConfigVO;
import pers.vic.boot.console.generator.model.TableEntity;
import pers.vic.boot.console.util.JsonUtil;

/**
 * 
 * @description:
 * @author VIC.xu
 * @date: 2020年1月10日 下午5:32:38
 */
@Service
public class GeneratorService {

	@Resource
	private ConfigurationOfGenerator configurationOfGenerator;

	@Resource
	private GeneratorMapper generatorMapper;

	public List<TableEntity> list(Lookup lookup) {
		List<TableEntity> datas = generatorMapper.queryList(lookup);
		return datas;
	}

	public PageInfo<TableEntity> page(Lookup lookup) {
		JsonUtil.printJson(lookup);
		PageHelper.startPage(lookup.getPage(), lookup.getSize());
		List<TableEntity> datas = generatorMapper.queryList(lookup);
		PageInfo<TableEntity> info = PageInfo.instance(datas);
		return info;
	}

	private List<ColumnEntity> queryColumns(String tableName) {
		return generatorMapper.queryColumns(tableName);
	}

	private TableEntity queryTable(String tableName) {
		return generatorMapper.queryTable(tableName);
	}

	/**
	 * 根据表名生成代码
	 * 
	 * @param moduleName
	 * @param packageName
	 */
	@Deprecated
	public byte[] generatorCode(String[] tableNames, String packageName, String moduleName) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ZipOutputStream zip = new ZipOutputStream(outputStream);

		for (String tableName : tableNames) {
			generatorCode(tableName, packageName, moduleName, zip);
		}
		IOUtils.closeQuietly(zip);
		return outputStream.toByteArray();
	}
	
	@Deprecated
	private void generatorCode(String tableName, String packageName, String moduleName, ZipOutputStream zip) {

		Map<String, Object> map = getTemplateData(tableName, packageName, moduleName);
		VelocityContext context = new VelocityContext(map);
		TableEntity table = buildTableDetail(tableName);
		// 获取模板列表
		List<String> templates = getTemplates();
		for (String template : templates) {
			// 渲染模板
			StringWriter sw = new StringWriter();
			Template tpl = Velocity.getTemplate(template, "UTF-8");
			tpl.merge(context, sw);
			try {
				// 添加到zip
				String fileName = getFileName(template, table.getClassName(), configurationOfGenerator.getPackageName(),
						configurationOfGenerator.getModuleName());
				zip.putNextEntry(new ZipEntry(fileName));
				IOUtils.write(sw.toString(), zip, "UTF-8");
				IOUtils.closeQuietly(sw);
				zip.closeEntry();
			} catch (Exception e) {
				throw new RuntimeException("渲染模板失败，表名：" + table.getTableName(), e);
			}

		}
	}

	/**
	 * 构建table的详情 包含的列的信息等
	 * @param tableName
	 * @return
	 */
	public TableEntity buildTableDetail(String tableName) {
		// 查询表信息
		TableEntity table = queryTable(tableName);
		if (table == null) {
			throw new RuntimeException("不存在的表信息");
		}
		// 查询列信息
		List<ColumnEntity> columns = queryColumns(tableName);
		// 配置信息
		Configuration config = getConfig();
		boolean hasBigDecimal = false;
		String className = tableToJava(table.getTableName(), configurationOfGenerator.getTablePrefix());

		table.setClassName(className);
		// 第一个字母小写
		table.setClassname(StringUtils.uncapitalize(className));

		// 被实体忽略的字段集合
		Set<String> ignoreColumns = configurationOfGenerator.getIgnores();
		// 列信息
		for (ColumnEntity column : columns) {
			// 列名转换成Java属性名
			String attrName = columnToJava(column.getColumnName());
			column.setAttrName(attrName);
			column.setAttrname(StringUtils.uncapitalize(attrName));

			// 列的数据类型，转换成Java类型
			// 此处并没有默认String为默认数据类型,所以没有配置的类型在java实体中会报错
			String attrType = config.getString(column.getDataType(), "unknowType");
			column.setAttrType(attrType);
			if (!hasBigDecimal && "BigDecimal".equals(attrType)) {
				hasBigDecimal = true;
			}
			if ("Date".equals(attrType)) {
				table.setHasDate(true);
			}
			// 此字段是否在实体中忽略(基类中已定义)
			if (ignoreColumns.contains(column.getColumnName())) {
				column.setEntityIgnore(true);
			}
			// 是否主键
			if ("PRI".equalsIgnoreCase(column.getColumnKey()) && table.getPk() == null) {
				table.setPk(column);
			}
			// 根据列是否是图片和日期 判断表中是否含有图片和日期

		}
		// 没主键，则第一个字段为主键
		table.setColumns(columns);
		if (table.getPk() == null) {
			table.setPk(columns.get(0));
		}
		return table;

	}

	// 获得渲染模板的数据
	private Map<String, Object> getTemplateData(String tableName, String packageName, String moduleName) {
		// 配置信息
		Configuration config = getConfig();

		TableEntity table = buildTableDetail(tableName);

		// 设置velocity资源加载器
		Properties prop = new Properties();
		prop.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		Velocity.init(prop);

		// 封装模板数据
		Map<String, Object> map = new HashMap<String, Object>(32);
		map.put("table", table);
		map.put("tableName", table.getTableName());
		map.put("comments", table.getComments());
		map.put("pk", table.getPk());
		map.put("className", table.getClassName());
		map.put("classname", table.getClassname());
		map.put("pathName", table.getClassname().toLowerCase());
		map.put("columns", table.getColumns());
		map.put("hasBigDecimal", table.getHasBigDecimal());
		if (StringUtils.isEmpty(packageName)) {
			map.put("package", config.getString("packageName"));
		} else {
			map.put("package", packageName);
		}
		if (StringUtils.isEmpty(packageName)) {
			map.put("moduleName", config.getString("moduleName"));
		} else {
			map.put("moduleName", moduleName);
		}
		map.put("author", config.getString("author"));
		map.put("email", config.getString("email"));
		map.put("datetime", DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm"));
		return map;
	}

	/**
	 * 获取文件名
	 */
	public static String getFileName(String template, String className, String packageName, String moduleName) {
		String packagePath = "generator" + File.separator + className;
		
		//TODO  修改为配置形式
		String entityVm = "Entity.java.vm";
		String looupVm = "Lookup.java.vm";
		String mapperVm = "Mapper.java.vm";
		
		// 实体
		if (template.contains(entityVm)) {
			return packagePath + File.separator + className + ".java";
		}
		// 查询条件
		if (template.contains(looupVm)) {
			return packagePath + File.separator + className + "Lookup.java";
		}

		// mapper.java
		if (template.contains(mapperVm)) {
			return packagePath + File.separator + className + "Mapper.java";
		}

		// service
		if (template.contains("Service.java.vm")) {
			return packagePath + File.separator + className + "Service.java";
		}

		// controller
		if (template.contains("Controller.java.vm")) {
			return packagePath + File.separator + className + "Controller.java";
		}
		// mapper.xnl
		if (template.contains("Mapper.xml.vm")) {
			return packagePath + File.separator + className + "Mapper.xml";
		}

		// list.jsp
		if (template.contains("list.jsp.vm")) {
			return packagePath + File.separator + "list.jsp";
		}
		// form.jsp
		if (template.contains("form.jsp.vm")) {
			return packagePath + File.separator + "form.jsp";
		}

		return null;
	}

	/**
	 * 获取要填充的模板
	 * 
	 * @return
	 */
	public List<String> getTemplates() {
		return configurationOfGenerator.getTemplates();
	}

	/**
	 * 表名转换成Java类名
	 */
	private String tableToJava(String tableName, String tablePrefix) {
		if (StringUtils.isNotBlank(tablePrefix)) {
			tableName = tableName.replaceFirst(tablePrefix, "");
		}
		return columnToJava(tableName);
	}

	/**
	 * 列名转换成Java属性名 is_delete -->delete
	 */
	public static String columnToJava(String columnName) {
//		return WordUtils.capitalizeFully(columnName, new char[] { '_' }).replace("_", "");//aaBBcc-->aabbcc  不是我想要的
		return underlineToCamel(columnName);
	}

	/** 下划线 */
	static String UNDERLINE = "_";
	/** 字段属性转java属性忽略的前缀 */
	static String IGNORE_COLUMN_PREFIX = "is_";

	/**
	 * 下划线转驼峰 但是不小写
	 * 
	 * @param str
	 * @return
	 */
	public static String underlineToCamel(String str) {
		if (StringUtils.isEmpty(str)) {
			return str;
		}
		int prefixLen = IGNORE_COLUMN_PREFIX.length();
		if (str.length() > prefixLen && str.substring(0, prefixLen).equalsIgnoreCase(IGNORE_COLUMN_PREFIX)) {
			str = str.substring(prefixLen);
		}
		str = str.toLowerCase();
		StringBuffer sb = new StringBuffer();
		for (String s : str.split(UNDERLINE)) {
			sb.append(StringUtils.capitalize(s));
		}
		return sb.toString();
	}

	/**
	 * 获取配置信息
	 */
	public static Configuration getConfig() {
		try {
			return new PropertiesConfiguration("config/generator-config.properties");
		} catch (ConfigurationException e) {
			throw new RuntimeException("获取配置文件失败，", e);
		}
	}

	public Map<String, String> findTableDetail(String tableName, String packageName, String moduleName) {
		// 查询表信息
		TableEntity table = queryTable(tableName);
		Map<String, Object> map = getTemplateData(tableName, packageName, moduleName);
		VelocityContext context = new VelocityContext(map);

		// 获取模板列表
		List<String> templates = getTemplates();
		Map<String, String> data = new LinkedHashMap<String, String>();
		for (String template : templates) {
			// 渲染模板
			StringWriter sw = new StringWriter();
			Template tpl = Velocity.getTemplate(template, "UTF-8");
			tpl.merge(context, sw);
			try {
				// 添加到zip
				String fileName = getFileName(template, table.getClassName(), configurationOfGenerator.getPackageName(),
						configurationOfGenerator.getModuleName());
				data.put(fileName.replace("generator" + File.separator, "").replace(File.separator, "_"),
						sw.toString());
			} catch (Exception e) {
				throw new RuntimeException("渲染模板失败，表名：" + table.getTableName(), e);
			}

		}
		return data;
	}

	/**
	 * 根据条件导出
	 * 
	 * @param tableConfigVO
	 */
	public byte[] exportByConfig(TableConfigVO tableConfigVO) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ZipOutputStream zip = new ZipOutputStream(outputStream);
		generatorCode(tableConfigVO, zip);
		IOUtils.closeQuietly(zip);
		return outputStream.toByteArray();
	}

	private void generatorCode(TableConfigVO tableConfigVO, ZipOutputStream zip) {
		TableEntity table = buildTableDetail(tableConfigVO.getTableName());
		Map<String, Object> map = getTemplateData(table, tableConfigVO);
		VelocityContext context = new VelocityContext(map);
		// 获取模板列表
		List<String> templates = getTemplates();
		for (String template : templates) {
			// 渲染模板
			StringWriter sw = new StringWriter();
			Template tpl = Velocity.getTemplate(template, "UTF-8");
			tpl.merge(context, sw);
			try {
				// 添加到zip
				String fileName = getFileName(template, table.getClassName(), configurationOfGenerator.getPackageName(),
						configurationOfGenerator.getModuleName());
				zip.putNextEntry(new ZipEntry(fileName));
				IOUtils.write(sw.toString(), zip, "UTF-8");
				IOUtils.closeQuietly(sw);
				zip.closeEntry();
			} catch (Exception e) {
				throw new RuntimeException("渲染模板失败，表名：" + table.getTableName(), e);
			}

		}
	}

	/**
	 * 通过前端配置构建模板数据
	 * 
	 * @param tableConfigVO
	 * @return
	 */
	private Map<String, Object> getTemplateData(TableEntity table, TableConfigVO tableConfigVO) {
		
		// 配置信息
		Configuration config = getConfig();
		String packageName = tableConfigVO.getPackageName();
		if (StringUtils.isEmpty(packageName)) {
			packageName = config.getString("packageName");
		}
		String moduleName = tableConfigVO.getModuleName();
		if (StringUtils.isEmpty(moduleName)) {
			packageName = config.getString("moduleName");
		}
		// 设置velocity资源加载器
		Properties prop = new Properties();
		prop.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		Velocity.init(prop);
		// 根据页面配置列的信息:是否列表  查询条件
		configColunm(table, tableConfigVO);
		// 封装模板数据
		Map<String, Object> map = new HashMap<String, Object>(32);
		map.put("table", table);
		map.put("tableName", table.getTableName());
		map.put("comments", table.getComments());
		map.put("pk", table.getPk());
		map.put("className", table.getClassName());
		map.put("classname", table.getClassname());
		map.put("pathName", table.getClassname().toLowerCase());
		map.put("columns", table.getColumns());
		map.put("hasBigDecimal", table.getHasBigDecimal());
		map.put("package", packageName);
		map.put("moduleName", moduleName);
		map.put("author", config.getString("author"));
		map.put("email", config.getString("email"));
		map.put("datetime", DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm"));
		return map;
	}

	/**
	 * 配置表的列信息
	 * @param table
	 * @param tableConfigVO
	 */
	private void configColunm(TableEntity table, TableConfigVO tableConfigVO) {
		if (table == null || tableConfigVO == null || table.getColumns() == null
				|| tableConfigVO.getColumns() == null) {
			return;
		}
		List<ColumnEntity> columns = table.getColumns();
		List<ColumnConfigVO> columnsConfig = tableConfigVO.getColumns();
		//列配置转为 列名->配置
		Map<String, ColumnConfigVO> columnsConfigMap= new HashMap<String, ColumnConfigVO>(32);
		for(ColumnConfigVO co : columnsConfig) {
			columnsConfigMap.put(co.getColumnName(), co);
		}
		for(ColumnEntity c : columns) {
			ColumnConfigVO config = columnsConfigMap.get(c.getColumnName());
			if(config != null) {
				c.getExtend().setShow( config.getShow())
				.setWhere(config.getShow())
				.setCondition(config.getCondition());			}
		}
	}

}
