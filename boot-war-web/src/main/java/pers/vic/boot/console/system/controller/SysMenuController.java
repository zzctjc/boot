package pers.vic.boot.console.system.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pers.vic.boot.base.controller.BaseController;
import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.console.system.model.SysMenu;
import pers.vic.boot.console.system.service.SysMenuService;

/** 
 * @description: 系统菜单controller
 * @author: Vic.xu
 * @date: 2019年11月19日 上午10:42:41
 */
@RestController
@RequestMapping(value = "/system/menu")
public class SysMenuController extends BaseController<SysMenuService, SysMenu>{

	/**
	 * 全部的菜单-树形结构
	 * @return
	 */
	@GetMapping(value = "menuTree")
	public BaseResponse menuTree() {
		List<SysMenu> tree = service.menuTree();
		return BaseResponse.success(tree);
	}
	
	/**
	 * 菜单列表-全部  (重写积累的list)
	 */
	@GetMapping(value="list")
	@Override
	public BaseResponse list(SysMenu lookup) {
		List<SysMenu> list = service.list(lookup);
		return BaseResponse.success(list);
	}
	
	/**
	 * 用作选择父级菜单: 不包含按钮
	 * @return
	 */
	@GetMapping(value = "chooseParent")
	public BaseResponse chooseParent() {
		List<SysMenu> list = service.chooseParent();
		return BaseResponse.success(list);
	}
	
}
