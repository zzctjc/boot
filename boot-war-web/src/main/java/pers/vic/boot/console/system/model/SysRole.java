package pers.vic.boot.console.system.model;

import pers.vic.boot.base.model.BaseEntity;

/**
 * 角色表 实体类
 * 
 * @author Vic.xu
 */
public class SysRole extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 角色名称
	 */
	private String name;

	/**
	 * 角色备注
	 */
	private String remark;

	/**
	 * set：角色名称
	 */
	public SysRole setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * get：角色名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * set：角色备注
	 */
	public SysRole setRemark(String remark) {
		this.remark = remark;
		return this;
	}

	/**
	 * get：角色备注
	 */
	public String getRemark() {
		return remark;
	}
}
