package pers.vic.boot.console.config.shiro.test;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.console.config.shiro.JwtUtil;
import pers.vic.boot.console.system.model.SysUser;
import pers.vic.boot.console.system.service.SysUserService;

/**
 * @description:
 * @author: Vic.xu
 * @date: 2019年12月31日 下午1:20:38
 */
@RestController
public class ShiroTestController {

	@Autowired
	private SysUserService sysUserService;

	@GetMapping("/login")
	public BaseResponse login(String username, String password) {
		SysUser user = sysUserService.findUserByUsername(username);
		String token = JwtUtil.sign(username, user.getPassword());
		return BaseResponse.success(token);
	}

	@GetMapping("/article")
	public BaseResponse article() {
		Subject subject = SecurityUtils.getSubject();
		if (subject.isAuthenticated()) {
			return BaseResponse.success("You are already logged in");
		} else {
			return BaseResponse.error();
		}
	}

	@GetMapping("/require_auth")
	@RequiresAuthentication
	public BaseResponse requireAuth() {
		return BaseResponse.success("You are authenticated");
	}

	@GetMapping("/require_role")
	@RequiresRoles("admin")
	public BaseResponse requireRole() {
		return BaseResponse.success("You are visiting require_role");
	}

	@GetMapping("/require_permission")
	@RequiresPermissions(logical = Logical.AND, value = { "view", "edit" })
	public BaseResponse requirePermission() {
		return BaseResponse.success("You are visiting permission require edit,view");
	}

	@RequestMapping(path = "/401")
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
	public BaseResponse unauthorized() {
		return BaseResponse.error("Unauthorized");
	}
}
