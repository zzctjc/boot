package pers.vic.boot.console.generator.mapper;

import java.util.List;

import pers.vic.boot.base.lookup.Lookup;
import pers.vic.boot.console.generator.model.ColumnEntity;
import pers.vic.boot.console.generator.model.TableEntity;


public interface GeneratorMapper {

	List<TableEntity> queryList(Lookup lookup);
	
	List<ColumnEntity> queryColumns(String tableName);
	
	TableEntity queryTable(String tableName);
	

}
