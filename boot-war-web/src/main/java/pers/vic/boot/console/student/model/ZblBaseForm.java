package pers.vic.boot.console.student.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import pers.vic.boot.base.model.BaseEntity;

/**
 * 表单基本信息 实体类
 * 
 * @author Vic.xu
 */
public class ZblBaseForm extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 表单名称
	 */
	private String name;

	/**
	 * 截止时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date endTime;

	/**
	 * 备注
	 */
	private String remark;

	/***************** set|get start **************************************/
	/**
	 * set：表单名称
	 */
	public ZblBaseForm setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * get：表单名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * set：截止时间
	 */
	public ZblBaseForm setEndTime(Date endTime) {
		this.endTime = endTime;
		return this;
	}

	/**
	 * get：截止时间
	 */
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * set：备注
	 */
	public ZblBaseForm setRemark(String remark) {
		this.remark = remark;
		return this;
	}

	/**
	 * get：备注
	 */
	public String getRemark() {
		return remark;
	}
	/***************** set|get end **************************************/
}
