package pers.vic.boot.console.test.mybatis.mapper;

import java.util.List;
import java.util.Map;

import pers.vic.boot.base.mapper.BaseMapper;
import pers.vic.boot.console.test.mybatis.model.TestMybatis;
/**
 * 
 * @description:
 * @author VIC.xu
 * @date: 2020年1月10日 下午5:57:21
 */
public interface TestMybatisMapper extends BaseMapper<TestMybatis>{
	
	/**
	 * 测试从oracle查询数据
	 * @description: 测试从oracle查询数据
	 * @return 
	 */
	List<Map<String, Object>> queryFromOracle();
}
