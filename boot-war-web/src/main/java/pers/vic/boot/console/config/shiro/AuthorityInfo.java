package pers.vic.boot.console.config.shiro;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import lombok.Data;

/**
 * @description:存放用户的一些权限信息
 * @author VIC.xu
 * @date: 2020年1月13日 上午10:13:15
 */
@Data
public class AuthorityInfo {
	
	private Integer userId;
	//用户名
	private String username;
	//拥有的角色
	private List<String> roles = new ArrayList<String>();
	//拥有的权限
	private List<String> permission = new ArrayList<String>();
	
	/**
	 * 开始检测是否需要提醒前端更换token
	 */
	private long expireCheckTime;
	
	/**
	 * @description: 是否需要提醒刷新
	 * @author: Vic.xu
	 * @date: 2020年1月14日 下午1:43:08
	 * @return
	 */
	public boolean needRefresh() {
		return expireCheckTime < System.currentTimeMillis();
	}
	
	public AuthorityInfo(Integer userId, String username, List<String> roles, List<String> permission) {
		super();
		this.userId = userId;
		this.username = username;
		this.roles = roles;
		this.permission = permission;
		 filterNull();
	}
	
	public AuthorityInfo(Integer userId, String username, String[] roles, String[] permission, long expireCheckTime) {
		super();
		this.userId = userId;
		this.username = username;
		this.expireCheckTime = expireCheckTime;
		this.roles = new ArrayList<String>(Arrays.asList(roles == null ? new String[] {} : roles));
		this.permission = new ArrayList<String>(Arrays.asList(permission == null ? new String[] {} : permission));
		 filterNull();
		
	}
	
	/**
	 * @description: 过滤掉空
	 * @author: Vic.xu
	 * @date: 2020年1月13日 下午5:15:44
	 */
	private void filterNull() {
		this.roles = filterEmptyItem(roles);
		this.permission = filterEmptyItem(permission);
	}
	
	/**
	 * 
	 * @description: 过滤
	 * @author: Vic.xu
	 * @date: 2020年1月13日 下午5:13:41
	 * @param list
	 * @return
	 */
	private static List<String> filterEmptyItem(List<String> list) {
		return Optional.ofNullable(list).orElse(new ArrayList<String>()).stream().filter(item->{
			return StringUtils.isNotEmpty(item);
		}).collect(Collectors.toList());
	}
	
	public AuthorityInfo() {
	}

	public Set<String> getRoleSet() {
		if(CollectionUtils.isNotEmpty(roles)) {
			return new HashSet<String>(roles);
		}
		return Collections.emptySet();
	}
	
	public Set<String> getPermissionSet() {
		if(CollectionUtils.isNotEmpty(permission)) {
			return new HashSet<String>(permission);
		}
		return Collections.emptySet();
	}
	
	
}
