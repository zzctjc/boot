package pers.vic.boot.console.develop.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pers.vic.boot.base.controller.BaseController;
import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.console.develop.model.DevelopPlan;
import pers.vic.boot.console.develop.service.DevelopPlanService;

/**
 * @description:开发计划 控制层
 * @author Vic.xu
 * @date: 2019-53-04 09:53
 */
@RestController
@RequestMapping("/develop/plan")
public class DevelopPlanController extends BaseController<DevelopPlanService, DevelopPlan>{

	@GetMapping(value = "list")
	@Override
	@RequiresPermissions(value = {"develop.plan.list"})
	public BaseResponse list(DevelopPlan lookup) {
		return super.list(lookup);
	}

	@PostMapping(value = "/save")
	@Override
	@RequiresPermissions(value = {"develop.plan.add"})
	public BaseResponse save(DevelopPlan entity) {
		return super.save(entity);
	}

	@Override
	@RequiresPermissions(value = {"develop.plan.detail"})
	public BaseResponse detail(Integer id) {
		return super.detail(id);
	}

	@Override
	@RequiresPermissions(value = {"develop.plan.delete"})
	public BaseResponse delete(int id) {
		return super.delete(id);
	}
	
	

	@Override
	@RequiresPermissions(value = {"develop.plan.deletes"})
	public BaseResponse delete(int[] ids) {
		return super.delete(ids);
	}

	
	
}
