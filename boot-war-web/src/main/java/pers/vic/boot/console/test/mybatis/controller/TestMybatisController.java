package pers.vic.boot.console.test.mybatis.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pers.vic.boot.base.controller.BaseController;
import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.base.model.PageInfo;
import pers.vic.boot.console.rpc.consumer.TestRpcService;
import pers.vic.boot.console.test.mybatis.model.TestMybatis;
import pers.vic.boot.console.test.mybatis.sevice.TestMybatisService;

/** 
 * @description:测试mybatis查询的controller 
 * @author: Vic.xu
 * @date: 2019年11月13日 下午2:40:41
 */
@RestController
@RequestMapping("/test/mybatis")
public class TestMybatisController extends BaseController<TestMybatisService, TestMybatis>{
	
	@Resource
	private TestRpcService testRpcService;

	@GetMapping("list")
	@Override
	public BaseResponse list(TestMybatis lookup) {
		PageInfo<TestMybatis> list = service.page(lookup);
		
		return BaseResponse.success(list);
	}
	
	@GetMapping("rpc")
	public BaseResponse rpc(@RequestParam(defaultValue = "2")Integer id) {
		testRpcService.test(id);
		
		return BaseResponse.success();
	}
	
	@GetMapping("addAge1")
	public BaseResponse addAge1(@RequestParam(defaultValue = "15")Integer id) {
		try {
			service.addAge1(id);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return BaseResponse.success();
	}
	
	@GetMapping("addAge2")
	public BaseResponse addAge2(@RequestParam(defaultValue = "15")Integer id) {
		try {
			service.addAge2(id);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return BaseResponse.success();
	}
	
	@GetMapping("addAge3")
	public BaseResponse addAge3(@RequestParam(defaultValue = "15")Integer id) {
		try {
			service.testTransactional();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return BaseResponse.success();
	}
	
	@GetMapping("addAge4")
	public BaseResponse addAge4(@RequestParam(defaultValue = "15")Integer id) {
		try {
			service.rollback(id);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return BaseResponse.success();
	}
}
