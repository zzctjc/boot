package pers.vic.boot.console.config.db;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import pers.vic.boot.console.mybatishelper.AuthorityHelper;


/** 
 * @description: 配置MYBATIS权限拦截器 参考#PageHelperAutoConfiguration
 * @author: Vic.xu
 * @date: 2020年2月11日 下午2:07:28
 */
@Configuration
public class MybatisConfig {
	
	@Autowired
    private List<SqlSessionFactory> sqlSessionFactoryList;


	/**
	 * @description: 配置MYBATIS权限拦截器
	 * @author: Vic.xu
	 * @date: 2020年2月11日 下午4:49:10
	 */
    @PostConstruct
    public void addAuthorityInterceptor() {
        AuthorityHelper interceptor = new AuthorityHelper();
        for (SqlSessionFactory sqlSessionFactory : sqlSessionFactoryList) {
            sqlSessionFactory.getConfiguration().addInterceptor(interceptor);
        }
    }

}
