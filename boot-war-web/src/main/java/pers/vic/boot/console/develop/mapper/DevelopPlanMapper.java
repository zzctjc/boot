package pers.vic.boot.console.develop.mapper;

import pers.vic.boot.base.mapper.BaseMapper;
import pers.vic.boot.console.develop.model.DevelopPlan;

/**
 * @description:开发计划 Mapper
 * @author Vic.xu
 * @date: 2019-53-04 09:53
 */
public interface DevelopPlanMapper extends BaseMapper<DevelopPlan> {

}
