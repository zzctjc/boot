package pers.vic.boot.console.system.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import per.vic.attachment.annotation.AttachmentFlag;
import per.vic.attachment.annotation.AttachmentFlag.AttachmenType;
import pers.vic.boot.base.model.BaseEntity;

/**
 * 系统用户表 实体类
 * 
 * @author Vic.xu
 */
public class SysUser extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 昵称
	 */
	private String nickname;

	/**
	 * 登录名
	 */
	private String username;

	/**
	 * 密码
	 */
	private String password;

	/**
	 * 性别 0-保密 1-男2-女
	 */
	private Integer gender;
	
	/**
	 * 头像  对应附件表id
	 */
	@AttachmentFlag(AttachmenType.SIGN)
	private Integer avatar;

	/**
	 * 生日
	 */
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date birthday;

	/* ***********************************************/
	// 用户拥有的角色
	private List<SysRole> roles;

	/* ***********************************************/

	public String getRoleIds() {
		if (CollectionUtils.isNotEmpty(roles)) {
			String roleIds = roles.stream().map(r->(r.getId()+"")).collect(Collectors.joining(","));
			return roleIds;
		}
		return "";
	}

	public static void main(String[] args) {
		List<BaseEntity> roles = new ArrayList<BaseEntity>();
		roles.add(new BaseEntity().setId(2));
		roles.add(new BaseEntity().setId(1));
		String ids = roles.stream().map(r -> r.getId() + "").collect(Collectors.joining(","));
		System.out.println(ids);
	}

	/**
	 * 邮箱
	 */
	private String email;

	/**
	 * 电话
	 */
	private String phone;

	/**
	 * set：昵称
	 */
	public SysUser setNickname(String nickname) {
		this.nickname = nickname;
		return this;
	}

	/**
	 * get：昵称
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * set：登录名
	 */
	public SysUser setUsername(String username) {
		this.username = username;
		return this;
	}

	/**
	 * get：登录名
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * set：密码
	 */
	public SysUser setPassword(String password) {
		this.password = password;
		return this;
	}

	/**
	 * get：密码
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * set：性别 0-保密 1-男2-女
	 */
	public SysUser setGender(Integer gender) {
		this.gender = gender;
		return this;
	}

	/**
	 * get：性别 0-保密 1-男2-女
	 */
	public Integer getGender() {
		return gender;
	}

	/**
	 * set：生日
	 */
	public SysUser setBirthday(Date birthday) {
		this.birthday = birthday;
		return this;
	}

	/**
	 * get：生日
	 */
	public Date getBirthday() {
		return birthday;
	}

	/**
	 * set：邮箱
	 */
	public SysUser setEmail(String email) {
		this.email = email;
		return this;
	}

	/**
	 * get：邮箱
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * set：电话
	 */
	public SysUser setPhone(String phone) {
		this.phone = phone;
		return this;
	}

	/**
	 * get：电话
	 */
	public String getPhone() {
		return phone;
	}
	
	

	public Integer getAvatar() {
		return avatar;
	}

	public void setAvatar(Integer avatar) {
		this.avatar = avatar;
	}

	public List<SysRole> getRoles() {
		return roles;
	}

	public void setRoles(List<SysRole> roles) {
		this.roles = roles;
	}

}
