package pers.vic.boot.console.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import pers.vic.boot.base.mapper.BaseMapper;
import pers.vic.boot.console.system.model.SysMenu;

/**
 * 菜单表 Mapper
 * 
 * @author Vic.xu
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

	/**
	 * 当前pid下的子菜单最大的code
	 * @param pid
	 * @return
	 */
	Long getMaxCode(@Param("pid")Integer pid);
	
	/**
	 * 用作选择当前菜单父级菜单
	 */
	List<SysMenu> chooseParent();
	
}
