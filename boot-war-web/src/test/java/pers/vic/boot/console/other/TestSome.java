package pers.vic.boot.console.other;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

import pers.vic.boot.util.RegexUtil;

/** 
 * @description: 
 * @author: Vic.xu
 * @date: 2019年11月22日 上午10:18:50
 */
public class TestSome {

	
	public static void main(String[] args) throws IOException {
		t1();
		/*
		 * String str = "values ('0350000114', '中国工商银行股份有限公司宾县支行', "; String reg =
		 * "values \\('(\\d+)', '(.*?)', "; RegexUtil.getList(str, reg, new
		 * int[]{1,2}).forEach(arr->{ System.out.println(String.join(" ", arr)); });;
		 */
	}
	
	public static void t1() throws IOException {
		List<String[]> result = new ArrayList<String[]>();
		String reg = "values \\('(\\d+)', '(.*?)', ";
		String f = "E:/1.sql";
		List<String> lines = FileUtils.readLines(new File(f));
		lines.forEach(str->{
			List<String[]> list = RegexUtil.getList(str, reg, new int[]{1,2});
			result.addAll(list);
		});
		System.out.println(result.size());
	}
}
