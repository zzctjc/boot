package pers.vic.boot.console;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;

@RunWith(SpringRunner.class)
@SpringBootTest
@Rollback(true)  
public class BaseTest {

	
	public static void main(String[] args) throws JSQLParserException {
		String sql = "select * from test_table where a=1 group by c";
		Select select = (Select) CCJSqlParserUtil.parse(sql);
		Expression where = CCJSqlParserUtil.parseCondExpression("a=1 and b=2");
		((PlainSelect) select.getSelectBody()).setWhere(where);
		System.out.println(select.toString());
	}

	@Test
	public void contextLoads() {
	}
}