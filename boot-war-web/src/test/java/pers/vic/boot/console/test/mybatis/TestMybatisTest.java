package pers.vic.boot.console.test.mybatis;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import pers.vic.boot.base.lookup.Lookup;
import pers.vic.boot.base.model.BaseResponse;
import pers.vic.boot.console.BaseTest;
import pers.vic.boot.console.test.mybatis.model.TestMybatis;
import pers.vic.boot.console.test.mybatis.sevice.TestMybatisService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Rollback(true)
public class TestMybatisTest extends BaseTest{
	
	@Resource
	private TestRestTemplate restTemplate;

	@Resource
	TestMybatisService testMybatisService;
	
	
	@Test
	public void addAge3() throws InterruptedException  {
		restTemplate.getForObject("/test/mybatis/addAge3", BaseResponse.class);

		
	}
	
	@Test
	public void addAge() throws InterruptedException  {
		restTemplate.getForObject("/test/mybatis/addAge1", BaseResponse.class);
		restTemplate.getForObject("/test/mybatis/addAge2", BaseResponse.class);

		
	}
	
	@Test
	public void list() {
		testMybatisService.list(new Lookup());
	}
	
	@Test
	public void insert() {
		for (int i = 0; i < 28; i++) {
			TestMybatis test = new TestMybatis();
			test.setName("test insert " + i);
			testMybatisService.insert(test);			
		}
	}
	
	@Test
	public void update() {
		TestMybatis test = new TestMybatis();
		test.setId(18);
		test.setName("我本修改了");
		testMybatisService.update(test);	
	}
	
	
	@Test
	public void queryFromOracle() {
		testMybatisService.queryFromOracle(true);
	}
}
