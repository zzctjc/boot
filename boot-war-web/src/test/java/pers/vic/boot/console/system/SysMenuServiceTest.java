package pers.vic.boot.console.system;

import javax.annotation.Resource;

import org.junit.Test;

import pers.vic.boot.base.lookup.Lookup;
import pers.vic.boot.console.BaseTest;
import pers.vic.boot.console.system.model.SysMenu;
import pers.vic.boot.console.system.service.SysMenuService;

/** 
 * @description: 
 * @author: Vic.xu
 * @date: 2019年11月15日 上午9:34:47
 */
public class SysMenuServiceTest extends BaseTest{
	
	@Resource
	SysMenuService sysMenuService;

	
	@Test
	public void list() {
		sysMenuService.list(new Lookup());
	}
	
	@Test
	public void add() {
		SysMenu entity = new SysMenu();
		entity.setPid(1);
		entity.setName("系统菜单");
		entity.setType(2);
		entity.setUrl("lcxm_system_user.html");
		entity.setSort(3);
		sysMenuService.save(entity);
		
	}
	
	
	
}
