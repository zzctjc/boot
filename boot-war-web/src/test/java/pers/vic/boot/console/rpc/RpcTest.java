package pers.vic.boot.console.rpc;

import javax.annotation.Resource;

import org.junit.Test;

import pers.vic.boot.console.BaseTest;
import pers.vic.boot.console.rpc.consumer.TestRpcService;

/** 
 * @description: 
 * @author: Vic.xu
 * @date: 2019年12月10日 下午12:36:44
 */
public class RpcTest extends BaseTest{

	@Resource
	private TestRpcService testRpcService;
	
	@Test
	public void t1() {
		testRpcService.test(111);
	}
	
	@Test
	public void t12() {
		testRpcService.test(2);
	}
}
