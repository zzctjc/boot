[TOC]

## 007-shiro+jwt

* https://zhuanlan.zhihu.com/p/29161098 : xml转config的过程
* [SpringBoot2.0+Shiro+JWT 整合](https://www.cnblogs.com/lr393993507/p/10644312.html)
* [Shiro + JWT + Spring Boot Restful 简易教程](https://www.jianshu.com/p/f37f8c295057)

### 一 shiro的认证和授权基本流程

#### 1.1认证流程

* **principals：**身份
* **credentials：**证明/凭证，
* **Subject**及**Realm**，分别是主体及验证主体的数据源。

![这里写图片描述](assets/20171012105949778.png)

1. 首先调用Subject.login(token)进行登录，其会自动委托给Security Manager，调用之前必须通过SecurityUtils. setSecurityManager()设置；
2. SecurityManager负责真正的身份验证逻辑；它会委托给Authenticator进行身份验证；
3. Authenticator才是真正的身份验证者，Shiro API中核心的身份认证入口点，此处可以自定义插入自己的实现； 
4. Authenticator可能会委托给相应的AuthenticationStrategy进行多Realm身份验证，默认ModularRealmAuthenticator会调用AuthenticationStrategy进行多Realm身份验证；
5. Authenticator会把相应的token传入Realm，从Realm获取身份验证信息，如果没有返回/抛出异常表示身份验证失败了。此处可以配置多个Realm，将按照相应的顺序及策略进行访问。

#### 1.2授权流程

* 主体（Subject）、
* 资源（Resource）、
* 权限（Permission）、
* 角色（Role）。
* ![这里写图片描述](assets/20171012110112635.png)

1. 首先调用Subject.isPermitted*/hasRole*接口，其会委托给SecurityManager，而SecurityManager接着会委托给Authorizer；

2. Authorizer是真正的授权者，如果我们调用如isPermitted(“user:view”)，其首先会通过PermissionResolver把字符串转换成相应的Permission实例； 

3. 在进行授权之前，其会调用相应的Realm获取Subject相应的角色/权限用于匹配传入的角色/权限；

4. Authorizer会判断Realm的角色/权限是否和传入的匹配，如果有多个Realm，会委托给ModularRealmAuthorizer进行循环判断，如果匹配如isPermitted*/hasRole*会返回true，否则返回false表示授权失败。

   ModularRealmAuthorizer进行多Realm匹配流程：

   ```
   1、首先检查相应的Realm是否实现了实现了Authorizer；
   2、如果实现了Authorizer，那么接着调用其相应的isPermitted*/hasRole*接口进行匹配；
   3、如果有一个Realm匹配那么将返回true，否则返回false。
   
   1、如果Realm进行授权的话，应该继承AuthorizingRealm，其流程是：
   （1）如果调用hasRole*，则直接获取AuthorizationInfo.getRoles()与传入的角色比较即可；
   （2）首先如果调用如isPermitted(“user:view”)，首先通过PermissionResolver将权限字符串转换成相应的Permission实例，默认使用WildcardPermissionResolver，即转换为通配符的WildcardPermission；
   2、通过AuthorizationInfo.getObjectPermissions()得到Permission实例集合；通过AuthorizationInfo. getStringPermissions()得到字符串集合并通过PermissionResolver解析为Permission实例；然后获取用户的角色，并通过RolePermissionResolver解析角色对应的权限集合（默认没有实现，可以自己提供）；
   3、接着调用Permission. implies(Permission p)逐个与传入的权限比较，如果有匹配的则返回true，否则false。
   
   ```

   

### [springboot集成shiro + jwt 代码](https://www.jianshu.com/p/0b1131be7ace)