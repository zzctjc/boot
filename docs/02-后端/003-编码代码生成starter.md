### 003-编码代码生成starter

>  说明： 初始的代码生成是直接放在[boot-war-web](https://gitee.com/lcxm/boot/tree/master/boot-war-web)项目的`pers.vic.boot.console.generator`模块下的， 个人觉得应该抽离成一个starter，更方便集成一些。

>  另：[boot-starter-generator](https://gitee.com/lcxm/boot/tree/master/boot-starter-generator) 已经实现了后端的基本代码， 尚未配置`spring.factories`，暂时只是一个独立的springboot课独立运行的项目。

#### 一 生成代码的基本思路

1. 连接数据库获取数据库表的全部信息(本项目暂只实现mysql版本)
   - 支持多种数据库,定义dao接口,不同的数据库做不同的实现,根据配置文件指定Primary
2. 解析表的基本信息,列的基本信息映射成对象
3. 编写各层模板(本项目使用的是**velocity**),根据解析的数据对模板进行填充
4. 导出填充后的模板

#### 二 [boot-starter-generator](https://gitee.com/lcxm/boot/tree/master/boot-starter-generator) 的简单说明

##### 1 配置文件 参考  `generator-config.properties`

```properties
#代码生成器，配置信息
#包名
generator.packageName=pers.vic.boot.console
#模块名
generator.moduleName=system
#作者
generator.author=Vic.xu
#Email
generator.email=xuduochoua@163.com
#表前缀(类名不会包含表前缀)
generator.tablePrefix=t_

#实体中忽略的字段(在基类中的字段) 另:列名转换成属性名的时候 is_delete格式会变成delete
#generator.ignores=id,is_enable,is_delete,create_time,update_time
generator.ignores=ID,CREATE_USER_ID,CREATE_TIME,LAST_UPDATE_USER_ID,LAST_UPDATE_TIME,ENABLE_FLAG


#需要填充的模板
generator.templates[0]=generator/templates/Entity.java.vm
generator.templates[1]=generator/templates/Mapper.java.vm
generator.templates[2]=generator/templates/Mapper.xml.vm
generator.templates[3]=generator/templates/Service.java.vm
generator.templates[4]=generator/templates/Controller.java.vm

#数据类型转换，配置信息:数据库类型->java类型
generator.dataTypeConvert.tinyint=Integer
generator.dataTypeConvert.smallint=Inteer
generator.dataTypeConvert.mediumint=Integer
generator.dataTypeConvert.int=Integer
generator.dataTypeConvert.integer=Integer
generator.dataTypeConvert.bigint=Long
generator.dataTypeConvert.float=Float
generator.dataTypeConvert.double=Double
generator.dataTypeConvert.decimal=BigDecimal
generator.dataTypeConvert.bit=Boolean
generator.dataTypeConvert.enum=String

generator.dataTypeConvert.char=String
generator.dataTypeConvert.varchar=String
generator.dataTypeConvert.tinytext=String
generator.dataTypeConvert.text=String
generator.dataTypeConvert.mediumtext=String
generator.dataTypeConvert.longtext=String

generator.dataTypeConvert.date=Date
generator.dataTypeConvert.time=Date
generator.dataTypeConvert.datetime=Date
generator.dataTypeConvert.timestamp=Date
```



##### 2 代码生成starter 入口 `GeneratorAutoconfigigure`

- 根据配置的`generator.database` 数据库类型， 注册不同的GeneratorDao实现类，本项目暂时已经实现了mysql版本的GeneratorDao；dao的对数据库的查询操作使用的是`jdbcTemplate`

```
/**
 * @description: 代码生成starter 入口
 * @author: Vic.xu
 * @date: 2020年3月10日 上午10:54:39
 */
@Configuration
@ConditionalOnClass(value = { JdbcTemplate.class })
@ConditionalOnWebApplication
@ConditionalOnExpression("${generatir.enabled:true}")
@EnableConfigurationProperties(GeneratorProperties.class)
public class GeneratorAutoconfigigure {

    /**
     * 本项目中支持的各个数据库类型
     */
    @Autowired
    private Collection<GeneratorDao> generatorDaoList;
    
    /**
     * 当前配置的数据库类型 默认MYSQL
     */
    @Value("${generator.database:mysql}")
    private String database;

    @Primary
    @Bean
    public GeneratorDao getGeneratorDao() {
        DatabaseType type = DatabaseType.getByName(database);
        Assert.notNull(type, "当前项目未支持" + database + "类型的数据库代码生成");
        GeneratorDao dao = null;
        for(GeneratorDao generatorDao : generatorDaoList) {
            if(type == generatorDao.databaseType()) {
                dao = generatorDao;
                break;
            }
        }
        Assert.notNull(dao, "当前项目未实现" + database + "类型的数据库的GeneratorDao");
        
        return dao;
    }

}

```



##### 3 测试代码生成`GeneratorTest`

> 暂时生成的zip中的文件是放在一个文件夹下，并未分包，可根据需要修改此逻辑

```
//查询当前数据库的表列表
public void list() {}
// 某个表的详情
public void detail() {}
// 导出某个或者多个表生成的
public void export() {}
```

##### 4 生成的文件包含

1. 实体依赖BaseEntity等

   ```java
   public class DevelopPlan extends BaseEntity {}
   ```

2. mybatis的xml，包含页面列查询，详情查询，新增，修改，删除等基本操作，参见BaseMapper接口

3. mapper：依赖BaseMapper

   ```java
   public interface DevelopPlanMapper extends BaseMapper<DevelopPlan> {
   }
   ```

   

4. service依赖BaseService

   ```java
   @Service
   public class DevelopPlanService extends BaseService<DevelopPlanMapper, DevelopPlan>{}
   ```

   

5. controller依赖BaseController

   ```java
   @RestController
   @RequestMapping("/develop/plan")
   public class DevelopPlanController extends BaseController<DevelopPlanService, DevelopPlan>{}
   ```

   

#### 三  (TODO)自动生成的starter的页面尚未开始做，建议使用`thymeleaf`编写,简单内置页面导出功能

##### TODO list

* 数据库的表列表页面
* 表列表页面多选，可多选导出基本的代码
* 表详情页面，根据配置的信息：包名/模块名/where条件 生成单表的相关代码文件

http://139.129.118.96/index.html  admin/123456 无验证码 开发管理->代码生成

![1585723070573](assets/1585723070573.png)

![1585723098512](assets/1585723098512.png)

![1585723111763](assets/1585723111763.png)



#### 四 关于凯捷微服务修改成独立应用页面配置数据库的方式

* 可基于本项目修改
* 其中依赖的BaseMapper BaseService 等当前放在commons-security， 望修改到commons-security-sdk,作为基础依赖存在；
* 数据库连接改为页面配置，可考虑放再页面配置完毕之后手动把JdbcTemplate 注册到spring，进而set到相关dao。