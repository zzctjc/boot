[TOC]



### 005-多数据源配置

#### 一 配置多个数据源

见代码:

```java
/** 
 * @description: 基本的数据源的配置
 * 需要配置DataSource、TransactionManager、SqlSessionFactory
 * (若非引入多数据源  此配置是不需要的 直接使用Spring自动装配的配置)
 * @author: Vic.xu
 * @date: 2019年12月18日 下午1:58:41
 */
@Configuration
@MapperScan(basePackages = BaseDataSourceConfig.PACKAGE, sqlSessionFactoryRef = "sqlSessionFactory")
public class BaseDataSourceConfig {
	//扫描的包
	public final static String PACKAGE="pers.vic.boot.console.**.mapper";
	//xml文件位置
	private static final String MAPPER_LOCALTION = "classpath:mapper/**/*.xml";
	
	/*
	 参见:
	 spring-boot-autoconfigure.{version}.jar
	 1.jdbc.DataSourceAutoConfiguration
	 */
	/**
	 * 把xxx的数据源指定为主数据源
	 * @return
	 */
	@Bean(name = "secondDataSource")
    @Primary
    @ConfigurationProperties(value = "spring.datasource")
    public DataSource dataSource() {
		DataSource dataSource = DataSourceBuilder.create().build();
//		DataSource dataSource = new HikariDataSource();
        return dataSource;
    }
	
	/**
	 * 创建  JdbcTemplate
	 */
	@Bean
	@Primary
	public JdbcTemplate jdbcTemplate(@Qualifier("secondDataSource")DataSource dataSource) {
	    return new JdbcTemplate(dataSource);
	}
	
	/**
	 * 默认的事物管理器  如果要创建第二个 需要指定DataSource 以及 事物管理器名称
	 */
	 @Bean(name = "masterTransactionManager")
    @Primary
    public DataSourceTransactionManager transactionManager(@Qualifier("secondDataSource")DataSource dataSource ) {
        return new DataSourceTransactionManager(dataSource);
    }
	
	/**
	 * 创建SqlSessionFactory
	 * @return
	 * @throws Exception
	 */
	@Bean
    @Primary
    public SqlSessionFactory sqlSessionFactory(@Qualifier("secondDataSource")DataSource dataSource) throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);
        //多数据源还是需要设置下此sqlSession扫描的mapper的位置
        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().getResources(MAPPER_LOCALTION));
        return sessionFactory.getObject();
    }

}
```



