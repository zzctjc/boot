[TOC]

## 004-编写附件attachment

[参考](https://juejin.im/post/5cc7c5c1f265da03a85accfd ) [参见](https://www.cnblogs.com/tjudzj/p/8758391.html)

### 1-计划

1. 附件starter:
2. 附件上传 下载  修改状态
3. 附件定时清理
4. 注解 告知附件是单个附件还是多个附件还是富文本中的附件
5. 注解 service里自动处理实体中包含的附件 save-> 持久态   updata: 游离态或者持久态  删除:  游离态或者持久态
6. 后台把id转为url:

### 2-先简单梳理下springboot的启动原理

#### 创建一个SpringApplication对象，并调用了run方法

#### 2.1 创建SpringApplication对象

1. 保存主配置:
   1. `this.primarySources = new LinkedHashSet<>(Arrays.asList(primarySources));`
2. 确定web应用类型。
   1. `this.webApplicationType = WebApplicationType.deduceFromClasspath();`
3. setInitializers方法从类路径下找到META-INF/spring.factories配置的所有ApplicationContextInitializer；然后保存起来
   1. setInitializers((Collection) getSpringFactoriesInstances(ApplicationContextInitializer.class));
4. 从类路径下找到ETA-INF/spring.factories配置的所有ApplicationListener
   1. setListeners((Collection) getSpringFactoriesInstances(ApplicationListener.class));
5. 从多个配置类中找到有main方法的主配置类
   1. this.mainApplicationClass = deduceMainApplicationClass();

#### 2.2run方法

1. 准备对象,略

2. 从类路径下META-INF/spring.factories获取SpringApplicationRunListeners，这个方法跟前面分析的两个获取配置方法类似。

   1. `SpringApplicationRunListeners listeners = getRunListeners(args);`

3. 回调所有的获取SpringApplicationRunListener.starting()方法

   1. `listeners.starting();`

4. 封装命令行参数

   1. ApplicationArguments applicationArguments = new DefaultApplicationArguments(
      args);

5. 准备环境，调用prepareEnvironment方法

   1. ConfigurableEnvironment environment = prepareEnvironment(listeners,applicationArguments);

6. 打印Banner图（就是启动时的标识图）。

   1. Banner printedBanner = printBanner(environment);

7. 创建ApplicationContext,决定创建web的ioc还是普通的ioc。

   1. context = createApplicationContext();

8. 异常分析报告。

   1. exceptionReporters = getSpringFactoriesInstances( SpringBootExceptionReporter.class, 		            new Class[] { ConfigurableApplicationContext.class }, context);

9. 准备上下文环境，将environment保存到ioc中，这个方法需要仔细分析下，我们再进入这个方法

   1. //applyInitializers()：回调之前保存的所有的ApplicationContextInitializer的initialize方法  		//listeners.contextPrepared(context)  		//prepareContext运行完成以后回调所有的SpringApplicationRunListener的contextLoaded（） 		prepareContext(context, environment, listeners, applicationArguments, 		            printedBanner);

10. 刷新容器,ioc容器初始化（如果是web应用还会创建嵌入式的Tomcat）,这个就是扫描，创建，加载所有组件的地方,（配置类，组件，自动配置）。

    ```java
    refreshContext(context);
    afterRefresh(context, applicationArguments);
    stopWatch.stop();
    if (this.logStartupInfo) {
        new StartupInfoLogger(this.mainApplicationClass)
            .logStarted(getApplicationLog(), stopWatch);
    }
    ```

    

11. 所有的SpringApplicationRunListener回调started方法。

    1. listeners.started(context);

12. 从ioc容器中获取所有的ApplicationRunner和CommandLineRunner进行回调，ApplicationRunner先回调，CommandLineRunner再回调。

    1. callRunners(context, applicationArguments);

13. 所有的SpringApplicationRunListener回调running方法。

    1. listeners.running(context);

14. 整个SpringBoot应用

15. 

16. 器。

### 3-如何自定义starter:场景启动器

**`确定依赖和编写自动配置`**

#### 自动装配的一些注解:

**@Configuration** ：指定这个类是一个配置类

**@ConditionalOnXXX** ：在指定条件成立的情况下自动配置类生效

**@AutoConfigureAfter**：指定自动配置类的顺序

**@Bean**：给容器中添加组件

**@ConfigurationPropertie**：结合相关xxxProperties类来绑定相关的配置

**@EnableConfigurationProperties**：让xxxProperties生效加入到容器中



按照这些注解写好自动配置类后，我们还需要进行自动配置的加载，加载方式是将需要启动就加载的自动配置类，配置在META-INF/spring.factories，启动器的大致原理是如此，而启动器的实际设计是有一定模式的，就是启动器模块是一个空 JAR 文件，仅提供辅助性依赖管理，而自动配置模块应该再重新设计一个，然后启动器再去引用这个自动配置模块。

Springboot就是如此设计的：

![img](assets/16a6c5cdab575c38)

另外还有一个命名规则：

**官方命名空间**

- 前缀：“spring-boot-starter-”
- 模式：spring-boot-starter-模块名
- 举例：spring-boot-starter-web、spring-boot-starter-actuator、spring-boot-starter-jdbc

 **自定义命名空间**

- 后缀：“-spring-boot-starter”
- 模式：模块-spring-boot-starter
- 举例：mybatis-spring-boot-starter



### 4 编写starter

**第一步**：因为我们需要创建两个模块，所以先新建一个空的项目，然后以模块形式创建两个模块。

**第二步**：再创建两个模块，一个starter和一个自动配置模块(去掉不需要的文件)

1. attachment-springboot-starter
2. attachment-springboot-starter-autoconfigurer

**第三步**：我们先将自动配置模块导入starter中，让启动模块依赖自动配置模块

```
<dependencies>
    <!--引入自动配置模块-->
    <dependency>
        <groupId>com.yuanqinnan-starter</groupId>
        <artifactId>yuanqinnan-springboot-starter-autoconfigurer</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>
</dependencies>
```

自动配置模块的完整POM文件：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.1.4.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>
    <groupId>com.yuanqinnan-starter</groupId>
    <artifactId>yuanqinnan-springboot-starter-autoconfigurer</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>jar</packaging>
    <properties>
        <java.version>1.8</java.version>
    </properties>
    <dependencies>
        <!--引入spring-boot-starter；所有starter的基本配置-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
        </dependency>
    </dependencies>
</project>
```

**第五步**：对自动配置类进行自动配置代码编写

 1. HelloProperties  略

 2. HelloService  略

 3. 将这个服务注入组件：

    ```java
    @Configuration
    @ConditionalOnWebApplication //web应用才生效
    @EnableConfigurationProperties(HelloProperties.class)
    public class HelloServiceAutoConfiguration {
    	@Autowired
    	    HelloProperties helloProperties;
    	@Bean
    	    public HelloService helloService(){
    		HelloService service = new HelloService();
    		service.setHelloProperties(helloProperties);
    		return service;
    	}
    }
    ```

**第六步**：对自动配置类进行自动配置代码编写 :将我们的自动配置类写入/META-INF/spring.factories

